---
id: getting-started
title: Getting Started
layout: docs
category: Quick Start
permalink: docs/getting-started.html
next: getting-started-linux
---

## Requirements

1. OS X - This guide assumes OS X which is needed for iOS development.
2. [Homebrew](http://brew.sh/) is the recommended way to install Watchman and Flow.
3. Install [Node.js](https://nodejs.org/) 4.0 or newer.
  - Install **nvm** with [its setup instructions here](https://github.com/creationix/nvm#installation). Then run `nvm install node && nvm alias default node`, which installs the latest version of Node.js and sets up your terminal so you can run it by typing `node`. With nvm you can install multiple versions of Node.js and easily switch between them.
  - New to [npm](https://docs.npmjs.com/)?
4. `brew install watchman`. We recommend installing [watchman](https://facebook.github.io/watchman/docs/install.html), otherwise you might hit a node file watching bug.
5. `brew install flow`, if you want to use [flow](http://www.flowtype.org).

We recommend periodically running `brew update && brew upgrade` to keep your programs up-to-date.

## iOS Setup

[Xcode](https://developer.apple.com/xcode/downloads/) 7.0 or higher is required. It can be installed from the App Store.

## Android Setup

To write React Native apps for Android, you will need to install the Android SDK (and an Android emulator if you want to work on your app without having to use a physical device). See [Android setup guide](docs/android-setup.html) for instructions on how to set up your Android environment.

_NOTE:_ There is experimental [Windows and Linux support](docs/linux-windows-support.html) for Android development.

## Quick start

Install the React Native command line tools:

    $ npm install -g react-native-cli

__NOTE__: If you see the error, `EACCES: permission denied`, please run the command: `sudo npm install -g react-native-cli`.

Create a React Native project:

    $ react-native init AwesomeProject


**To run the iOS app:**

- `$ cd AwesomeProject`
- `$ react-native run-ios` **OR** open `ios/AwesomeProject.xcodeproj` and hit "Run" button in Xcode
- Open `index.ios.js` in your text editor of choice and edit some lines.
- Hit ⌘-R in your iOS simulator to reload the app and see your change!

_Note: If you are using an iOS device, see the [Running on iOS Device page](docs/running-on-device-ios.html#content)._

**To run the Android app:**

- `$ cd AwesomeProject`
- `$ react-native run-android`
- Open `index.android.js` in your text editor of choice and edit some lines.
- Press the menu button (F2/⌘-M by default, depending on AVD version, or ⌘-M in Genymotion) and select *Reload JS*  (or press R twice) to see your change!
- Run `adb logcat *:S ReactNative:V ReactNativeJS:V` in a terminal to see your app's logs

_Note: If you are using an Android device, see the [Running on Android Device page](docs/running-on-device-android.html#content)._

Congratulations! You've successfully run and modified your first React Native app.

_If you run into any issues getting started, see the [troubleshooting page](docs/troubleshooting.html#content)._

## Adding Android to an existing React Native project

If you already have a (iOS-only) React Native project and want to add Android support, you need to execute the following commands in your existing project directory:

1. Update the `react-native` dependency in your `package.json` file to [the latest version](https://www.npmjs.com/package/react-native)
2. `$ npm install`
3. `$ react-native android`

## Linux and Windows Support 
NOTE: This guide focuses on Android development. You'll need a Mac to build iOS apps.

As React Native on iOS requires a Mac and most of the engineers at Facebook and contributors use Macs, support for OS X is a top priority. However, we would like to support developers using Linux and Windows too. We believe we'll get the best Linux and Windows support from people using these operating systems on a daily basis.

Therefore, Linux and Windows support for the development environment is an ongoing community responsibility. This can mean filing issues and submitting PRs, and we'll help review and merge them. We are looking forward to your contributions and appreciate your patience.

As of version 0.14 Android development with React native is mostly possible on Linux and Windows. You'll need to install Node.js 4.0 or newer. On Linux we recommend installing watchman, otherwise you might hit a node file watching bug.

## What's missing on Windows 
On Windows the packager won't be started automatically when you run react-native run-android. You can start it manually using:

    $ cd MyAwesomeApp
    $ react-native start

If you hit a ERROR Watcher took too long to load on Windows, try increasing the timeout in this file (under your node_modules/react-native/).


##ANDROID NOTE
Gradle needs to be 2.10 or higher.  Also needed to bundle as follows:
`curl "http://localhost:8081/index.android.bundle?platform=android" -o "android/app/src/main/assets/index.android.bundle"`
