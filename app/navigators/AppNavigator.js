import React from 'react';
import { Platform,TouchableOpacity } from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator, DrawerNavigator } from 'react-navigation';

//Drawer icon
import Icon from 'react-native-vector-icons/FontAwesome';
import DrawerContent from '../components/common/DrawerContent';
//Pages
import Account from '../components/user/Account';
import CustomerMenu from '../components/menu/CustomerMenu';
import CustomerMap from '../components/map/CustomerMap';
import Landing from '../components/common/Landing';
import AppLoading from '../components/common/AppLoading';
import Loading from '../components/common/Loading';
import Login from '../components/login/Login';
// import LaundryVroomMenu from '../components/LaundryVroomMenu';
import OrderHistory from '../components/user/OrderHistory';
import OrderLineHistory from '../components/user/OrderLineHistory';
import OrderSummary from '../components/order/OrderSummary';
import OrderItemInstructions from '../components/order/OrderItemInstructions';
import OrderSuccess from '../components/order/OrderSuccess';
import Payment from '../components/order/Payment';
import ProductSelection from '../components/order/ProductSelection';
import Privacy from '../components/legal/Privacy';
import Pricing from '../components/pricing/Pricing';
import Registration from '../components/registration/Registration';
import Terms from '../components/legal/Terms';

const LaundryVroomColors = require('LaundryVroomColors');
const navigationOptions = {
  headerStyle: {
    backgroundColor:LaundryVroomColors.darkBackground,
    borderBottomColor:LaundryVroomColors.lightText,
    borderBottomWidth:1,
  },
  headerTitleStyle: {
    color:"#fff"
  },
  headerBackTitleStyle: {
    color:"#fff"
  },
  headerTintColor:'#FFF',
  gesturesEnabled: false,
};

const ModalStack ={
  OrderItemInstructions: {
    screen: OrderItemInstructions,
    navigationOptions: ({navigation}) => (navigationOptions),
  }
}

const LoginRoutes = {
  Login: {
    screen: Login,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
};
const RegisterRoutes = {
  Register: {
    screen: Registration,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
};
const ModalStackNavigator = StackNavigator(ModalStack, {
  mode: 'modal',
});
const Stack = {
  Account: {
    screen: Account,
    navigationOptions: ({navigation}) => (navigationOptions),
  },
  CustomerMap: {
    screen: CustomerMap,
    // navigationOptions: ({navigation}) => (navigationOptions),
  },
  Landing: {
    screen: Landing,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  AppLoading: { screen: AppLoading, },
  Loading: { screen: Loading, },
  OrderHistory: {
    screen: OrderHistory,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  OrderLineHistory: {
    screen: OrderLineHistory,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  OrderSummary: {
    screen: OrderSummary,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  OrderSuccess: {
    screen: OrderSuccess,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  Payment: {
    screen: Payment,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  ProductSelection: {
    screen: ProductSelection,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  Privacy: {
    screen: Privacy,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  Pricing: {
    screen: Pricing,
    navigationOptions: ({navigation}) => (navigationOptions)
  },
  Terms: {
    screen: Terms,
    navigationOptions: ({navigation}) => (navigationOptions)
  }
};

const DrawerRoutes = {
	CustomerMapStack: {
		name: 'CustomerMapStack',
		screen: StackNavigator(Stack,
       {
         initialRouteName: 'CustomerMap',
         navigationOptions: ({navigation}) => ({
           title:"Confirm Location",
           drawerLabel:"Request Service",
           drawerIcon: ({ tintColor }) => (
             <Icon name="map" size={25} color={LaundryVroomColors.darkBackground} />
           ),
           headerLeft:(
             <TouchableOpacity
                onPress={() => navigation.navigate('DrawerOpen')}
                style={{"marginLeft":10}}
              >
                <Icon name="bars" size={30} color="#fff" />
             </TouchableOpacity>
           ),
           headerStyle: {
             backgroundColor:LaundryVroomColors.darkBackground,
             borderBottomColor:LaundryVroomColors.lightText,
             borderBottomWidth:1,
           },
           headerTitleStyle: {
             color:"#fff"
           },
           headerBackTitleStyle: {
             color:"#fff"
           },
           headerTintColor:'#FFF'
        }),
      })
	},
	AccountStack: {
		name: 'AccountStack',
		screen: StackNavigator(Stack, {
      initialRouteName: 'Account',
      navigationOptions: ({navigation}) => ({
        drawerLabel: "Account",
        drawerIcon: ({ tintColor }) => (
          <Icon name="user" size={25} color={LaundryVroomColors.darkBackground} />
        )
      })
    })
	},
	OrderHistoryStack: {
		name: 'OrderHistoryStack',
		screen: StackNavigator(Stack, {
       initialRouteName: 'OrderHistory',
       navigationOptions: ({navigation}) => ({
         drawerLabel: "Order History",
         drawerIcon: ({ tintColor }) => (
           <Icon name="history" size={25} color={LaundryVroomColors.darkBackground} />
         )
       })
    })
	},
  PricingStack: {
    name: 'PricingStack',
    screen: StackNavigator(Stack, {
       initialRouteName: 'Pricing',
       navigationOptions: ({navigation}) => ({
         drawerLabel: "Pricing",
         drawerIcon: ({ tintColor }) => (
           <Icon name="tag" size={25} color={LaundryVroomColors.darkBackground} />
         )
       })
   })
  },
  TermsStack: {
    name: 'TermsStack',
    screen: StackNavigator(Stack, {
      initialRouteName: 'Terms',
      navigationOptions: ({navigation}) => ({
        drawerLabel: "Terms",
        drawerIcon: ({ tintColor }) => (
          <Icon name="gavel" size={25} color={LaundryVroomColors.darkBackground} />
        )
      })
   })
  },
  PrivacyStack: {
    name: 'PrivacyStack',
    screen: StackNavigator(Stack, {
      initialRouteName: 'Privacy',
      navigationOptions: ({navigation}) => ({
        drawerLabel: "Privacy",
        drawerIcon: ({ tintColor }) => (
          <Icon name="user-secret" size={25} color={LaundryVroomColors.darkBackground} />
        )
      })
   })
  },
};

export const AppNavigator = StackNavigator({
    Drawer: {
  			name: 'Drawer',
  			screen: DrawerNavigator(
  				DrawerRoutes,
          {
            contentComponent:DrawerContent,
            contentOptions: {
              activeTintColor:"#000"
            },
            drawerWidth:275
          }
          // {headerMode:"none"}
  			),
		},
    InstructionsModal: {
        name:"InstructionsModal",
        screen: ModalStackNavigator
    },
    LoginScreen: {
      name:"LoginScreen",
      screen: StackNavigator(
        LoginRoutes
      )
    },
    RegisterScreen: {
      name:"RegisterScreen",
      screen: StackNavigator(
        RegisterRoutes,
      )
    },
  	...Stack,

},
{
  initialRouteName: 'AppLoading',
  headerMode: 'none',
  mode: 'card',
});

const AppWithNavigationState = ({ dispatch, nav }) => (
  <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
);

AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);
