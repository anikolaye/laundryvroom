/**
 *
 * @providesModule LaundryVroomToolbar
 * @flow
 */
import React, {
  Component
} from 'react';

import {
  ActivityIndicator,
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

import Icon from 'react-native-vector-icons/Octicons';

var LaundryVroomColors = require('LaundryVroomColors');

class LaundryVroomToolbar extends Component {
  props: {
    asImage:false;
    leftText:string;
    centerText: string;
    rightText: string;
    rightLoading:false;
    style: any;
    includeDirectionIcon: false;
    onLeftPress: () => void;
    onRightPress: () => void;
  };

  render() {
    const leftCaption = this.props.leftText.toUpperCase();
    const centerCaption = this.props.centerText.toUpperCase();

    if(this.props.rightLoading) {
      var rightCaption = this._renderActivity();
    } else {
      var rightCaption = this._renderRightText(this.props.rightText.toUpperCase());
    }
    if(this.props.asImage) {
      return (
        <View style={styles.toolbar}>
          <View style={styles.center}>
            <LvText style={styles.toolbarTitle}>{centerCaption}</LvText>
          </View>
          <TouchableOpacity onPress={this.props.onLeftPress}>
            <View style={styles.left}>
              <Icon style={styles.rightArrowButton} name="x" size={35} color={LaundryVroomColors.backgroundColor}/>
            </View>
          </TouchableOpacity>
          <View style={styles.right}>
            <LvText onPress={this.props.onRightPress} style={[styles.toolbarButton, styles.toolbarBold]}>{rightCaption}</LvText>
          </View>
        </View>
      );
    } else {
      return (
        <View style={[styles.toolbar, styles.extraHeight]}>
          <View style={styles.center}>
            <LvText style={styles.toolbarTitle}>{centerCaption}</LvText>
          </View>
          <TouchableOpacity onPress={this.props.onLeftPress}>
            <View style={styles.left}>
              <LvText style={styles.toolbarCaption}>
                {this.props.includeDirectionIcon ? (
                  <Icon style={styles.leftArrowText} name="arrow-left" size={15} color={LaundryVroomColors.backgroundColor}/>
                  ) : null
                }
                {leftCaption}
              </LvText>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.props.onRightPress}>
            <View style={styles.right}>
              {rightCaption}
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  }

  _renderRightText(caption) {
    if(caption) {
      return(
        <LvText style={styles.toolbarCaption}>
          {caption}
          {this.props.includeDirectionIcon ? (
            <Icon style={styles.leftArrowText} name="arrow-right" size={15} color={LaundryVroomColors.backgroundColor}/>
            ) : null
          }
        </LvText>
      );
    } else {
      return null;
    }
  }

  _renderActivity() {
    return (
      <View style={styles.right}>
         <ActivityIndicator animating={true} color={LaundryVroomColors.backgroundColor} size="small"/>
      </View>
    )
  }
}

var styles = StyleSheet.create({
  toolbar:{
    // flex:1,
    flexDirection:'row',
    justifyContent: 'space-between',
    // height:50,
    backgroundColor:LaundryVroomColors.darkBackground,
    paddingTop:30,
    paddingBottom:10,
    borderBottomColor:LaundryVroomColors.lightText,
    borderBottomWidth:1
  },
  extraHeight: {
    height:70
  },
  left: {
    // flex:1,
    // flexDirection:'row',
    justifyContent:'flex-start',
    alignSelf:'center',
    alignItems:'center',
    marginLeft:10,
    marginTop:5
  },
  leftArrowText: {
    marginRight:5
  },
  center: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 20,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  right: {
    // flex:1,
    // flexDirection:'row',
    justifyContent:'flex-end',
    alignSelf:'center',
    alignItems:'center',
    paddingRight:10,
    marginTop:5
  },
  rightArrowText: {
    marginLeft:5
  },
  toolbarImage:{
    // marginTop:20,
    width:30,
    height:30
  },
  toolbarButton:{
    marginLeft:10,
    width:60,
    letterSpacing: -1,
    color:'#fff',
  },
  toolbarCaption:{
    // width:60,
    // letterSpacing: -1,
    color:'#fff',
    paddingLeft:3,
    paddingRight:3
  },
  toolbarTitle:{
    fontSize:16,
    color:'#fff',
    fontWeight:'bold',
  },
  toolbarBold: {
    // paddingTop:2,
    fontWeight:'bold'
  }
});

module.exports = LaundryVroomToolbar;
