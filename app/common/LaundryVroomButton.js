/**
 * Copyright 2016 Facebook, Inc.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * Facebook.
 *
 * As with any software that integrates with the Facebook platform, your use
 * of this software is subject to the Facebook Developer Principles and
 * Policies [http://developers.facebook.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE
 *
 * @providesModule LaundryVroomButton
 * @flow
 */

'use strict';
import React, {
  Component
} from 'react';

import {
  ActivityIndicator,
  Image,
  StyleSheet,
  PixelRatio,
  TouchableOpacity,
  View
} from 'react-native'
var { LvText } = require('LaundryVroomText');

var LaundryVroomColors = require('LaundryVroomColors');

class LaundryVroomButton extends Component {
  props: {
    type: 'primary' | 'secondary' | 'bordered';
    icon: number;
    caption: string;
    style: any;
    onPress: () => void;
  };

  render() {
    const caption = this.props.caption.toUpperCase();
    let icon;
    let loading;
    if (this.props.icon) {
      icon = <Icon name={this.props.icon.name} size={this.props.icon.size} color={this.props.icon.color} />;

      // icon = <Image source={this.props.icon} style={styles.icon} />;
    }
    let content;
    if (this.props.type === 'submit') {
      content = (
        <LinearGradient
          start={[0.5, 1]} end={[1, 1]}
          colors={['#6A6AD5', '#6F86D9']}
          style={[styles.button, styles.submitButton]}>
          {icon}
          {loading}
          <LvText style={[styles.caption, styles.submitCaption]}>
            {caption}
          </LvText>
        </LinearGradient>
      );
    } else if (this.props.type === 'light') {
        if (this.props.loading) {
          loading = <ActivityIndicator color={LaundryVroomColors.darkBackground} />;
        }
        content = (
          <View
            start={[0.5, 1]} end={[1, 1]}
            colors={[LaundryVroomColors.lightBackground, LaundryVroomColors.lightText]}
            style={[styles.button, styles.border, styles.lightButton]}>
            {icon}
            {loading}
            <LvText style={[styles.caption, styles.lightCaption]}>
              {caption}
            </LvText>
          </View>
        );
    } else if(this.props.type === 'inactive') {
      if (this.props.loading) {
        loading = <ActivityIndicator color={LaundryVroomColors.backgroundColor} />;
      }
      content = (
        <View
          start={[0.5, 1]} end={[1, 1]}
          colors={['#6A6AD5', '#6F86D9']}
          style={[styles.button, styles.inactiveButton]}>
          {icon}
          {loading}
          <LvText style={[styles.caption, styles.primaryCaption]}>
            {caption}
          </LvText>
        </View>
      );
    } else if (this.props.type === 'primary' || this.props.type === undefined) {
      if (this.props.loading) {
        loading = <ActivityIndicator color={LaundryVroomColors.backgroundColor} />;
      }
      content = (
        <View
          start={[0.5, 1]} end={[1, 1]}
          colors={['#6A6AD5', '#6F86D9']}
          style={[styles.button, styles.primaryButton]}>
          {icon}
          {loading}
          <LvText style={[styles.caption, styles.primaryCaption]}>
            {caption}
          </LvText>
        </View>
      );
    } else {
      var border = this.props.type === 'bordered' && styles.border;
      content = (
        <View style={[styles.button, border]}>
          {icon}
          <LvText style={[styles.caption, styles.secondaryCaption]}>
            {caption}
          </LvText>
        </View>
      );
    }
    return (
      <TouchableOpacity
        accessibilityTraits="button"
        onPress={this.props.onPress}
        activeOpacity={0.8}
        style={[styles.container, this.props.style]}>
        {content}
      </TouchableOpacity>
    );
  }
}

const HEIGHT = 50;

var styles = StyleSheet.create({
  container: {
    height: HEIGHT,
    // borderRadius: HEIGHT / 2,
    // borderWidth: 1 / PixelRatio.get(),
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40,
    borderColor: LaundryVroomColors.lightText,
  },
  border: {
    borderWidth: 1,
    borderColor: LaundryVroomColors.lightText,
    //borderRadius: HEIGHT / 2,
  },
  inactiveButton: {
    //borderRadius: HEIGHT / 2,
    backgroundColor:LaundryVroomColors.inactiveText,
  },
  primaryButton: {
    //borderRadius: HEIGHT / 2,
    backgroundColor:LaundryVroomColors.darkBackground,
  },
  lightButton: {
    //borderRadius: HEIGHT / 2,
    backgroundColor:LaundryVroomColors.lightBackground
  },
  submitButton: {
    backgroundColor:LaundryVroomColors.darkBackground
  },
  icon: {
    marginRight: 12,
  },
  caption: {
    letterSpacing: 1,
    fontSize: 12,
    color:"#fff",
    fontWeight:'bold',
  },
  lightCaption: {
    color: LaundryVroomColors.darkText
  },
  primaryCaption: {
    color: '#fff',
  },
  submitCaption: {
    color: 'white',
    fontWeight:'bold'
  },
  secondaryCaption: {
    color: LaundryVroomColors.lightText,
  },
  darkBackground: {
    backgroundColor:LaundryVroomColors.darkBackground,
    color:LaundryVroomColors.lightText,
    fontWeight:'bold'
  }
});

module.exports = LaundryVroomButton;
