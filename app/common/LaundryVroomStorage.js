/**
 * @providesModule LaundryVroomStorage
 * @flow
 */

'use strict';
import {
  AsyncStorage
} from 'react-native';

async function setStorageItem(key, value) {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.log('AsyncStorage error: ' + error.message);
  }
}

async function getStorageItem(key) {
  try {
    var value = await AsyncStorage.getItem(key);
    return value;
  } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
}

async function removeStorageItem(key) {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {
    console.log('AsyncStorage error: ' + error.message);
  }
}
module.exports = {
  setStorageItem,
  getStorageItem,
  removeStorageItem
}
