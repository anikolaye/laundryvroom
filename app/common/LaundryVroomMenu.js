/**
 *
 * @providesModule LaundryVroomMenu
 * @flow
 */
 'use strict';
 import React, {
   Component,
   PropTypes
 } from 'react';

 import {
   AppRegistry,
   StyleSheet,
   View,
   TextInput,
   TouchableOpacity,
   Image
 } from 'react-native';

var { LvText } = require('LaundryVroomText');
import Icon from 'react-native-vector-icons/FontAwesome';

const LaundryVroomColors = require('LaundryVroomColors');

class LaundryVroomMenu extends Component {
  static propTypes = {
    openDrawer: PropTypes.func.isRequired
  };

  render() {
    let {openDrawer} = this.props;
    const middleText = this.props.middleText.toUpperCase();
    const rightCaption= (this.props.rightCatpion ? this.props.rightCatpion : '');
    return (
      <View style={styles.toolbar}>
        <View style={styles.center}>
          <LvText style={styles.toolbarTitle}>{middleText}</LvText>
        </View>
        <TouchableOpacity onPress={openDrawer}>
          <View style={styles.left}>
            <Icon style={styles.rightArrowButton} name="bars" size={30} color={LaundryVroomColors.backgroundColor}/>
          </View>
        </TouchableOpacity>
        <View style={styles.right}>
          <LvText onPress={this.props.onRightPress} style={[styles.toolbarButton, styles.toolbarBold]}>{rightCaption}</LvText>
        </View>
      </View>
    );
  };

}

var styles = StyleSheet.create({
  toolbar:{
    // flex:1,
    flexDirection:'row',
    justifyContent: 'space-between',
    // height:50,
    backgroundColor:LaundryVroomColors.darkBackground,
    paddingTop:30,
    paddingBottom:10,
    borderBottomColor:LaundryVroomColors.lightText,
    borderBottomWidth:1
  },
  left: {
    // flex:1,
    // flexDirection:'row',
    justifyContent:'flex-start',
    alignSelf:'flex-start',
    // alignItems:'center',
    marginLeft:10
  },
  center: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 20,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  right: {
    // flex:1,
    // flexDirection:'row',
    justifyContent:'flex-end',
    alignSelf:'flex-end',
    alignItems:'flex-end'
  },
  toolbarImage:{
    // marginTop:20,
    width:30,
    height:30
  },
  toolbarButton:{
    width:60,
    letterSpacing: -1,
    color:'#fff',
  },
  toolbarTitle:{
    fontSize:16,
    color:'#fff',
    fontWeight:'bold',
  },
  toolbarBold: {
    // paddingTop:2,
    fontWeight:'bold'
  }
});

module.exports = LaundryVroomMenu;
