'use strict';
const {userURL, DEVICE_ID, STORAGE_KEY, USER_ID} = require('../env');
const baseURL = userURL + '/user-service-api/v1';
import type { Action } from './types';
import type { User } from '../reducers/user';
const LaundryVroomStorage = require('LaundryVroomStorage');

async function authenticate(email: String, password: String): Promise<Action> {
  try{
     let response = await fetch(`${baseURL}/authentication/login`, {
       method: "POST",
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json'
       },
       body: JSON.stringify({
         username: email,
         password: password,
         deviceId: DEVICE_ID
       })
     });
     let responseJson = await response.json();
     if(responseJson && responseJson.id) {
       await LaundryVroomStorage.setStorageItem(STORAGE_KEY, `${responseJson.token}`);
       return {
         type: 'LOADED_USER',
         user: responseJson
       };

     } else {
       return {
         type: 'LOADED_USER',
         user: responseJson
       };
    }
   } catch(error) {
     // Handle error
     console.error(error);
   }
 }

 async function register(user : User): Promise<Action> {
   try{
      let response = await fetch(`${baseURL}/authentication/register`, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
      });
      let responseJson = await response.json();
      if(responseJson && responseJson.id) {
        await LaundryVroomStorage.setStorageItem(STORAGE_KEY, `${responseJson.token}`);
        return {
          type: 'CREATE_USER',
          user: responseJson
        };

      } else {
        return {
          type: 'CREATE_USER',
          user: responseJson
        };
     }
    } catch(error) {
      // Handle error
      console.error(error);
    }
  }

async function logout() {
  try {
    LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
    LaundryVroomStorage.removeStorageItem(USER_ID);
    return {
      type: 'UNSET_USER',
      user: {}
    };
  } catch (error) {
    console.log('AsyncStorage error: ' + error.message);
  }
}

module.exports = {
  authenticate,
  register,
  logout
};
