'use strict';
const {merchantURL, STORAGE_KEY, USER_ID} = require('../env');
const LaundryVroomStorage = require('LaundryVroomStorage');
const baseURL = merchantURL + '/merchant-service-api/v1';
import type { Action } from './types';
import type { MerchantLocation } from '../reducers/merchantLocation';

async function findMerchants(lat: float, lon: float): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/merchant-location?lat=${lat}&lon=${lon}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      var returnJson = {error: true, errorMessage:"Oops!  We aren't in this area just yet!"};
      if(responseJson && responseJson.length > 0) {
        var returnJson = {error: true, errorMessage:"It looks like all of our laundromats are currently closed.  Please try again in the morning!"};
        for(var i =0; i < responseJson.length; i++) {
          if(responseJson[i].currentlyOpen) {
            returnJson = responseJson[i];
            break;
          }
        }
      }
      return {
        type: 'LOADED_MERCHANT_LOCATION',
        merchantLocation: returnJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}


async function loadMerchantLocation(merchantLocationid: integer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/merchant-location/${merchantLocationid}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_MERCHANT_LOCATION',
        merchantLocation: responseJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

module.exports = {
  findMerchants,
  loadMerchantLocation
};
