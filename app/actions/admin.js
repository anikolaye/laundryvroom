/**
 * Get the current app version state
 */
 const {adminUrl} = require('../env');
 const baseURL = adminUrl + '/admin-service-api/v1';
 import type { Action } from './types';
 import type { Admin } from '../reducers/admin';
