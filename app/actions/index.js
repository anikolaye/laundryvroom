'use strict';

//const loginActions = require('./login');
const adminActions = require('./admin');
const customerActions = require('./customer');
const merchantActions = require('./merchant');
const orderActions = require('./order');
const paymentActions = require('./payment');
const productActions = require('./product');
const userActions = require('./user');
module.exports = {
  ...adminActions,
  ...customerActions,
  ...merchantActions,
  ...orderActions,
  ...paymentActions,
  ...productActions,
  ...userActions
};
