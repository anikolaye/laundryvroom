'use strict';
const {productURL, STORAGE_KEY, USER_ID} = require('../env');
const LaundryVroomStorage = require('LaundryVroomStorage');
const baseURL = productURL + '/product-service-api/v1';
import type { Action, ProductSku } from './types';
import type { Product } from '../reducers/product';

async function findProducts(sku: String): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      var url = `${baseURL}/product`;
      if(sku) {
        url += `${url}?sku=${sku}`;
      }
      let response = await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }

      return {
        type: 'LOADED_PRODUCTS',
        products: responseJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function loadProduct(id: integer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/product/${id}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_PRODUCT',
        product: responseJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function createProduct(product: Product): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/product`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(product)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'CREATE_PRODUCT',
        productFees: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function updateProduct(id: integer, product: Product): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      if(!product.id || product.id != id) {
        throw new Exception('Product id is wrong');
      }
      let response = await fetch(`${baseURL}/product/${id}`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(productFees)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'UPDATE_PRODUCT',
        productFees: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

module.exports = {
  findProducts,
  loadProduct,
  createProduct,
  updateProduct
};
