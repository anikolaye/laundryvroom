'use strict';
const {orderURL, STORAGE_KEY, USER_ID} = require('../env');
const LaundryVroomStorage = require('LaundryVroomStorage');
const baseURL = orderURL + '/order-service-api/v1';
import type { Action} from './types';
import type { Payment, PaymentMethod } from '../reducers/payment';

async function loadPaymentToken(): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/order/payment/token`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_PAYMENT_TOKEN',
        payment: responseJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function createPaymentMethod(paymentMethod:PaymentMethod): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/order/payment/paymentMethod`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(paymentMethod)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'CREATE_PAYMENT_METHOD',
        paymentMethod: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function sendPayment(payment:Payment): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/order/payment/capture`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(payment)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'SEND_PAYMENT',
        payment: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

module.exports = {
  createPaymentMethod,
  loadPaymentToken,
  sendPayment
};
