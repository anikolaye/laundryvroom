'use strict';
const {orderURL, STORAGE_KEY, USER_ID} = require('../env');
const LaundryVroomStorage = require('LaundryVroomStorage');
const baseURL = orderURL + '/order-service-api/v1';

import type { Action, OrderStatus } from './types';
import type { Order } from '../reducers/order';
import type { OrderLines } from '../reducers/orderLine';

async function loadOrder(id: integer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/order/${id}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_ORDER',
        order: responseJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function findOrders(customerId: integer, status:String): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      var statusString = '';
      if(status && status != '') {
        statusString = `&status=${status}`;
      }
      let response = await fetch(`${baseURL}/order?requesterId=${customerId}${statusString}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_ORDERS',
        orders: responseJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}
async function createOrder(order: Order): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/order`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(order)
      });
      let responseJson = await response.json();

      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }

      return {
        type: 'CREATE_ORDER',
        order: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function updateOrder(id: integer, order: Order): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      if(!order.id || order.id != id) {
        throw new Exception('Order id is wrong');
      }
      let response = await fetch(`${baseURL}/order/${id}`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(order)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'UPDATE_ORDER',
        order: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function resetOrder() : Promise<Action> {
  return {
    type: 'LOAD_ORDER',
    order: {},
  };
}

async function loadOrderLines(orderId: integer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/order/orderLine?orderId=${orderId}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_ORDER_LINES',
        orderLines: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function createOrderLines(orderId: integer, orderLines: OrderLines): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      if(!orderLines.orderId || orderLines.orderId != orderId) {
        orderLines.orderId = orderId;
      }

      let response = await fetch(`${baseURL}/order/orderLine?orderId=${orderId}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(orderLines)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'CREATE_ORDER_LINES',
        orderLines: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function updateOrderLine(id: integer, orderId: integer, orderLine: OrderLine): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      if(!orderLines.orderId || orderLines.orderId != orderId) {
        throw new Exception('Order Id is wrong');
      }
      let response = await fetch(`${baseURL}/order/orderLine/${id}?orderId=${orderId}`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(orderLines)
      });
      let responseJson = await response.json();

      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }

      return {
        type: 'UPDATE_ORDER_LINE',
        orderLines: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function resetOrderLines() : Promise<Action> {
    return {
      type: 'LOADED_ORDER_LINES',
      order: {},
    };
}

module.exports = {
  loadOrder,
  createOrder,
  updateOrder,
  resetOrder,
  resetOrderLines,
  loadOrderLines,
  createOrderLines,
  updateOrderLine,
  findOrders
};
