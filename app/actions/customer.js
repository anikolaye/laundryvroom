'use strict';
const {customerURL, STORAGE_KEY, USER_ID} = require('../env');
const baseURL = customerURL + '/customer-service-api/v1';
const LaundryVroomStorage = require('LaundryVroomStorage');
import type { Action } from './types';
import type { Customer } from '../reducers/customer';
import type { CustomerPreferences } from '../reducers/customerPreferences';

async function loadCustomer(id: integer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/customer/${id}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_CUSTOMER',
        customer: responseJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function loadCustomerByUserId(userId: integer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/customer?userId=${userId}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_CUSTOMER',
        customer: responseJson
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function createCustomer(customer: Customer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/customer`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(customer)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'CREATE_CUSTOMER',
        customer: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function updateCustomer(id: integer, customer: Customer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      if(!customer.id || customer.id != id) {
        throw new Exception('Customer id is wrong');
      }
      let response = await fetch(`${baseURL}/customer/${id}`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(customer)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'UPDATE_CUSTOMER',
        customer: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function loadCustomerPreferences(customerId: integer): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      let response = await fetch(`${baseURL}/customer-preferences?customerId=${customerId}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'LOADED_CUSTOMER_PREFERENCES',
        customerPreferences: responseJson[0],
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}
async function createCustomerPreferences(customerId: integer, customerPreferences: CustomerPreferences): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      if(!customerPreferences.customerId || customerPreferences.customerId != customerId) {
        customerPreferences.customerId = customerId;
      }

      let response = await fetch(`${baseURL}/customer-preferences`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(customerPreferences)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'CREATE_CUSTOMER_PREFERENCES',
        customerPreferences: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

async function updateCustomerPreferences(id: integer, customerId: integer, customerPreferences: CustomerPreferences): Promise<Action> {
  try {
    var token = await LaundryVroomStorage.getStorageItem(STORAGE_KEY);
    if(!token) {
      LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
      LaundryVroomStorage.removeStorageItem(USER_ID);
    } else {
      if(!customerPreferences.customerId || customerPreferences.customerId != customerId) {
        throw new Exception('Customer Id is wrong');
      }
      let response = await fetch(`${baseURL}/customer-preferences/${id}`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(customerPreferences)
      });
      let responseJson = await response.json();
      if(responseJson.message != null && responseJson.message == "Unauthorized") {
        LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
        LaundryVroomStorage.removeStorageItem(USER_ID);
      }
      return {
        type: 'UPDATE_CUSTOMER_PREFERENCES',
        customerPreferences: responseJson,
      };
    }
  } catch(error) {
    // Handle error
    console.error(error);
  }
}

module.exports = {
  loadCustomer,
  loadCustomerByUserId,
  createCustomer,
  updateCustomer,
  loadCustomerPreferences,
  createCustomerPreferences,
  updateCustomerPreferences
};
