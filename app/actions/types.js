/**
 * Copyright 2016 Facebook, Inc.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * Facebook.
 *
 * As with any software that integrates with the Facebook platform, your use
 * of this software is subject to the Facebook Developer Principles and
 * Policies [http://developers.facebook.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE
 *
 * @flow
 */

'use strict';

type ParseObject = Object;
type Customer = Object;
type CustomerPreferences = Object;
type Merchant = Object;
type MerchantLocation = Object;
type Payment = Object;
type PaymentMethod = Object;
type Product = Object;
type Products = Object;
type Order = Object;
type Orders = Object;
type OrderLine = Object;
type OrderLines = Object;
type User = Object;

export type Action =
    { type: 'LOADED_CUSTOMER', customer: Customer }
  | { type: 'LOADED_CUSTOMER_PREFERENCES', customerPreferences: CustomerPreferences}
  | { type: 'CREATE_CUSTOMER', customer: Customer }
  | { type: 'CREATE_CUSTOMER_PREFERENCES', customerPreferences: CustomerPreferences}
  | { type: 'UPDATE_CUSTOMER', customer: Customer }
  | { type: 'UPDATE_CUSTOMER_PREFERENCES', customerPreferences: CustomerPreferences}
  | { type: 'LOADED_MERCHANT', merchant:Merchant}
  | { type: 'CREATE_MERCHANT', merchant:Merchant}
  | { type: 'UPDATE_MERCHANT', merchant:Merchant}
  | { type: 'LOADED_MERCHANT_LOCATION', merchantLocation:MerchantLocation}
  | { type: 'CREATE_MERCHANT_LOCATION', merchantLocation:MerchantLocation}
  | { type: 'UPDATE_MERCHANT_LOCATION', merchantLocation:MerchantLocation}
  | { type: 'LOADED_ORDER', order:Order}
  | { type: 'LOADED_ORDERS', orders:Orders}
  | { type: 'CREATE_ORDER', order:Order}
  | { type: 'UPDATE_ORDER', order:Order}
  | { type: 'LOADED_ORDER_LINES', orderLines:OrderLines}
  | { type: 'CREATE_ORDER_LINES', orderLines:OrderLines}
  | { type: 'UPDATE_ORDER_LINE', orderLine:OrderLine}
  | { type: 'LOADED_PAYMENT_TOKEN', payment:Payment}
  | { type: 'SEND_PAYMENT', payment:Payment}
  | { type: 'CREATE_PAYMENT_METHOD', paymentMethod:PaymentMethod}
  | { type: 'LOADED_PRODUCT', product: Product}
  | { type: 'LOADED_PRODUCT', products: Products}
  | { type: 'CREATE_USER', user: User}
  | { type: 'LOADED_USER', user: User}
  | { type: 'UNSET_USER', user: User}
  ;

export type Dispatch = (action: Action | ThunkAction | PromiseAction | Array<Action>) => any;
export type GetState = () => Object;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;
