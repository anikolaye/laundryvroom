import type {Action} from '../actions/types';

export type MerchantLocation = {
  id: ?int;
  merchantId: ?int;
  merchantLocationHours: MerchantLocationHours;
  acceptingOrders: ?bool;
  headquarters: ?bool;
  storeName: ?string;
  storePhone: ?string;
  storeAddress: ?Address;
  currentlyOpen: ?bool;
};

export type Address = {
  addressLine1: ?string;
  addressLine2: ?string;
  city: ?string;
  state: ?string;
  zip: ?string;
  country: ?string;
  latitude: ?float;
  longitude: ?float;
}

export type Address = {
  addressLine1: ?string;
  addressLine2: ?string;
  city: ?string;
  state: ?string;
  zip: ?string;
  country: ?string;
  latitude: ?float;
  longitude: ?float;
}

const initialState = {
  id: null,
  merchantId: null,
  acceptingOrders: null,
  headquarters: null,
  storeName: null,
  storePhone: null,
  storeAddress: null,
  currentlyOpen: null
};

type State = MerchantLocation;

function merchantLocation(state: State = initialState, action: Action): State {
  if (action.type === 'LOADED_MERCHANT_LOCATION') {
    return action.merchantLocation;
  }
  return state;
}

module.exports = merchantLocation;
