import type {Action} from '../actions/types';

export type Product = {
  id: ?int;
  name: ?string;
  description: ?string;
  effectiveDate: ?string;
  endDate: ?string;
};


type State = Product;

const initialState = {
  id: null,
  name: null,
  description: null,
  effectiveDate: null,
  endDate: null,
};

function product(state: State = initialState, action: Action): State {
  if (action.type === 'LOADED_PRODUCT') {
    return action.product;
  }
  if (action.type === 'CREATE_PRODUCT') {
    return action.product;
  }
  if (action.type === 'UPDATE_PRODUCT') {
    return action.product;
  }
  return state;
}

module.exports = product;
