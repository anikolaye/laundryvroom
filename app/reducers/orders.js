import type {Action} from '../actions/types';

export type Orders = {
  id: ?int;
  invoiceId: ?string;
  price: ?float;
  status: ?string;
  type: ?string;
  billingAddress: ?Address;
  requesterId: int;
  created:int;
  lastModified:int;
};

export type Address = {
  addressLine1: ?string;
  addressLine2: ?string;
  city: ?string;
  state: ?string;
  zip: ?string;
  country: ?string;
  latitude: ?float;
  longitude: ?float;
}

type State = Orders;

const initialState = {
  id: null,
  invoiceId: null,
  price: null,
  status: null,
  type: null,
  billingAddress: null,
  requesterId: null,
  created:null,
  lastModified:null
};

function orders(state: State = initialState, action: Action): State {
  if(action.type === 'LOADED_ORDERS') {
    return action.orders;
  }
  return state;
}

module.exports = orders;
