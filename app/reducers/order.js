import type {Action} from '../actions/types';

export type Order = {
  id: ?int;
  invoiceId: ?string;
  price: ?float;
  status: ?string;
  type: ?string;
  billingAddress: ?Address;
  requesterId: int;
  created:int;
  lastModified:int;
};

export type Address = {
  addressLine1: ?string;
  addressLine2: ?string;
  city: ?string;
  state: ?string;
  zip: ?string;
  country: ?string;
  latitude: ?float;
  longitude: ?float;
}

type State = Order;

const initialState = {
  id: null,
  invoiceId: null,
  price: null,
  status: null,
  type: null,
  billingAddress: null,
  requesterId: null,
  created:null,
  lastModified:null
};

function order(state: State = initialState, action: Action): State {
  if (action.type === 'LOADED_ORDER' ) {
    return action.order;
  }

  if (action.type === 'CREATE_ORDER') {
    return action.order;
  }
  if (action.type === 'UPDATE_ORDER') {
    return action.order;
  }
  return state;
}

module.exports = order;
