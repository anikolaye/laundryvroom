import type {Action} from '../actions/types';

export type Products = {
  id: ?int;
  name: ?string;
  description: ?string;
  effectiveDate: ?string;
  endDate: ?string;
};


type State = Products;

const initialState = {
  id: null,
  name: null,
  description: null,
  effectiveDate: null,
  endDate: null,
};

function products(state: State = initialState, action: Action): State {
  if (action.type === 'LOADED_PRODUCTS') {
    return action.products;
  }
  return state;
}

module.exports = products;
