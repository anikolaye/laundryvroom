import type {Action} from '../actions/types';

export type OrderLine = {
  id: ?int;
  orderId: ?int;
  customerId: int;
  merchantLocationId: int;
  price: ?float;
  status: ?String;
  orderLineProduct: ?OrderLineProduct;
  destinationAddress: ?Address;
  returnAddress: ?Address;
};

export type Address = {
  addressLine1: ?string;
  addressLine2: ?string;
  city: ?string;
  state: ?string;
  zip: ?string;
  country: ?string;
  latitude: ?float;
  longitude: ?float;
}

export type OrderLineProduct = {
  id: ?int;
  orderLineProductAttributes: ?OrderLineProductAttribute;
  sku: ?string;
  name: ?string;
  description: ?string;
  effectiveDate: ?string;
}

export type OrderLineProductAttribute = {
  id: ?int;
  key: ?string;
  value: ?string;
}

type State = OrderLine;

const initialState = {
  id: null,
  orderId: null,
  customerId: null,
  merchantLocationId: null,
  price: null,
  status: null,
  orderLineProduct: null,
  destinationAddress: null,
  returnAddress: null
};

export function orderLine(state: State = initialState, action: Action): State {
  if (action.type === 'UPDATE_ORDER_LINE') {
    return action.orderLine;
  }
  return state;
}

// export function orderLines(state: State = initialState, action: Action): State {
//   if (action.type === 'LOADED_ORDER_LINES') {
//     return action.orderLines;
//   }
//   if (action.type === 'CREATE_ORDER_LINES') {
//     return action.orderLines;
//   }
//   return state;
// }

module.exports = orderLine;
