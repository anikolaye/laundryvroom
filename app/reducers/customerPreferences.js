import type {Action} from '../actions/types';

export type CustomerPreferences = {
  id: ?int;
  customerId: ?int;
  detergentType: ?string;
  mensDressShirts: ?string;
  dropOffAtDoor: ?boolean;
};

const initialState = {
  id: null,
  customerId: null,
  detergentType: null,
  mensDressShirts: null,
  dropOffAtDoor: null
};

type State = CustomerPreferences;

function customerPreferences(state: State = initialState, action: Action): State {
  if (action.type === 'LOADED_CUSTOMER_PREFERENCES') {
    return action.customerPreferences;
  }
  if (action.type === 'CREATE_CUSTOMER_PREFERENCES') {
    return action.customerPreferences;
  }
  if (action.type === 'UPDATE_CUSTOMER_PREFERENCES') {
    return action.customerPreferences;
  }
  return state;
}

module.exports = customerPreferences;
