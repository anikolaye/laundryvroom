import type {Action} from '../actions/types';

export type PaymentAudit = {
  paymentStatus: ?string;
  rawResponse: ?string;
}

export type PaymentMethod = {
  id: ?int;
  paymentAudit:?PaymentAudit;
  transactionId: ?string;
  externalOrderId: ?string;
  transactionId: ?string;
  token:?string;
  nonce: ?string;
  customerId: ?string;
  firstName: ?string;
  lastName: ?string;
  company: ?string;
  email: ?string;
  fax: ?string;
  phone: ?string;
};

type State = PaymentMethod;

const initialState = {
  id: null,
  paymentAudit:null,
  transactionId: null,
  externalOrderId: null,
  transactionId: null,
  token:null,
  nonce: null,
  customerId: null,
  firstName: null,
  lastName: null,
  company: null,
  email: null,
  fax: null,
  phone: null,
};

function paymentMethod(state: State = initialState, action: Action): State {
  if (action.type === 'CREATE_PAYMENT_METHOD') {
    return action.paymentMethod;
  }
  return state;
}

module.exports = paymentMethod;
