'use strict';

var { combineReducers } = require('redux');

import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../navigators/AppNavigator';

// Start with two routes: The Main screen, with the Login screen on top.
const firstAction = AppNavigator.router.getActionForPathAndParams('Loading');
const tempNavState = AppNavigator.router.getStateForAction(firstAction);
// const secondAction = AppNavigator.router.getActionForPathAndParams('Landing');
const initialNavState = AppNavigator.router.getStateForAction(
  // secondAction,
  tempNavState
);

function nav(state = initialNavState, action) {
  let nextState;
  switch (action.type) {
    case 'Login':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.back(),
        state
      );
      break;
    case 'Logout':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Landing' }),
        state
      );
      break;
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}

const initialAuthState = { isLoggedIn: false };

function auth(state = initialAuthState, action) {
  switch (action.type) {
    case 'Login':
      return { ...state, isLoggedIn: true };
    case 'Logout':
      return { ...state, isLoggedIn: false };
    default:
      return state;
  }
}


const AppReducer = combineReducers({
  customer: require('./customer'),
  customerPreferences: require('./customerPreferences'),
  merchantLocation: require('./merchantLocation'),
  order: require('./order'),
  orders: require('./orders'),
  orderLine: require('./orderLine'),
  orderLines: require('./orderLines'),
  payment: require('./payment'),
  paymentMethod: require('./paymentMethod'),
  product: require('./product'),
  products: require('./products'),
  user: require('./user'),
  nav
});

export default AppReducer;
