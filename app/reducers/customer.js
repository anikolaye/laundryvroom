import type {Action} from '../actions/types';

export type Customer = {
  id: ?int;
  firstName: ?string;
  lastName: ?string;
  email: ?string;
  phone: ?string;
  gender: ?string;
  shippingAddress: ?Address;
  billingAddress: ?Address;
};

export type Address = {
  addressLine1: ?string;
  addressLine2: ?string;
  city: ?string;
  state: ?string;
  zip: ?string;
  country: ?string;
  latitude: ?float;
  longitude: ?float;
}

type State = Customer;

const initialState = {
  id:null,
  firstName: null,
  lastName: null,
  email: null,
  phoneNumber: null,
  gender: null,
  shippingAddress: null,
  billingAddress: null,
};

function customer(state: State = initialState, action: Action): State {
  if (action.type === 'LOADED_CUSTOMER') {
    return action.customer;
  }
  if (action.type === 'CREATE_CUSTOMER') {
    return action.customer;
  }
  if (action.type === 'UPDATE_CUSTOMER') {
    return action.customer;
  }
  return state;
}

module.exports = customer;
