'use strict';

import type {Action} from '../actions/types';

/*
private Integer id;
private String username; //User name is an email address
private String password;
private String fullName;
private Set<UserRoleDTO> userRoles;
private Date created;
private Date lastModified;
*/
export type User = {
  id: int;
  username: String;
  fullName: String;
  userRoles: ?Array;
};

type State = User;

const initialState = {
  id: null,
  username: null,
  fullName: null,
  userRoles: null
};

function user(state: State = initialState, action: Action): State {
    if (action.type === 'LOADED_USER') {
      return action.user;
    }
    if (action.type === 'CREATE_USER') {
      return action.user;
    }
    if (action.type === 'UNSET_USER') {
      return action.user;
    }
    return state;
}

module.exports = user;
