import type {Action} from '../actions/types';

export type Payment = {
  amount: ?string;
  nonce: ?string;
  transactionId: ?string;
  token: ?string;
};


type State = Payment;

const initialState = {
  amount: null,
  nonce: null,
  transactionId: null,
  token: null,
};

function payment(state: State = initialState, action: Action): State {
  if (action.type === 'LOADED_PAYMENT_TOKEN') {
    return action.payment;
  }
  if (action.type === 'SEND_PAYMENT') {
    return action.payment;
  }
  return state;
}

module.exports = payment;
