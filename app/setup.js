import React from 'react';
import { AppState, AppRegistry, StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import LaundryVroomLoader from './LaundryVroomLoader';
import Loading from './components/common/Loading';
var configureStore = require('./store/configureStore');

class LaundryVroomApp extends React.Component {
  constructor() {
    console.disableYellowBox = true;
    super();
    this.state = {
      isLoading: true,
      store: configureStore(() => this.setState({isLoading: false})),
    };
  }

  render() {
    if (this.state.isLoading) {
        return <Loading />;
    }
    return (
      <Provider store={this.state.store}>
        <LaundryVroomLoader />
      </Provider>
    );
  }
}

AppRegistry.registerComponent('LaundryVroom', () => LaundryVroomApp);

export default LaundryVroomApp;
