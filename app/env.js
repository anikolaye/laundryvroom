/**
 * Copyright 2016 Facebook, Inc.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * Facebook.
 *
 * As with any software that integrates with the Facebook platform, your use
 * of this software is subject to the Facebook Developer Principles and
 * Policies [http://developers.facebook.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE
 *
 * @flow
 */

'use strict';

var DeviceInfo = require('react-native-device-info');
const LOCAL_URL = 'localhost:8000';
const PROD_URL = 'api.laundryvroom.com:8000';
const CURRENT_URL = LOCAL_URL;

const LOCAL_USER_URL = 'localhost:9000';
const PROD_USER_URL = 'api.laundryvroom.com:9000';
const CURRENT_USER_URL = PROD_USER_URL;

module.exports = {
  userURL: `http://${CURRENT_URL}`,
  customerURL:`http://${CURRENT_URL}`,
  driverURL: `http://${CURRENT_URL}`,
  merchantURL:`http://${CURRENT_URL}`,
  productURL: `http://${CURRENT_URL}`,
  orderURL:`http://${CURRENT_URL}`,
  version: 1.0,
  DEVICE_ID: DeviceInfo.getUniqueID(),
  STORAGE_KEY: 'token',
  USER_ID:'user_id',
  MAPS_API_KEY:'AIzaSyBCwKxy-rNiLT7XTYnWqM0sh7O-dgZujo4'
};
