'use strict';

import React, {
  Component
} from 'react';

import {
  AppRegistry,
  AppState,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  PropTypes,
  AsyncStorage
} from 'react-native';

import { NavigationActions } from 'react-navigation';
import AppWithNavigationState from './navigators/AppNavigator';

var { STORAGE_KEY, USER_ID } = require('./env');
const LaundryVroomStorage = require('LaundryVroomStorage');
var {
  loadCustomer,
  loadCustomerPreferences,
  findOrders,
  findProducts
} = require('./actions');
const { connect } = require('react-redux');

class LaundryVroomLoader extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn:false,
      loading:true
    }
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    this.resetViews();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange(appState) {
    if (appState === 'active') {
      // this.resetViews();
    }
  }

  resetViews() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      if(customerId !== null) {
        this.setState({isLoggedIn: true});
        this.props.dispatch(loadCustomer(customerId));
        this.props.dispatch(loadCustomerPreferences(customerId));
        this.props.dispatch(findOrders(customerId, "PROCESSING"));
        this.props.dispatch(findProducts());
      } else {
        this.setState({isLoggedIn: false});
      }
    }.bind(this));
  }

  render() {
    return (<AppWithNavigationState />);
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});


function select(state) {
  return {
    customer: state.customer,
    customerPreferences: state.customerPreferences,
    order: state.order,
    products: state.products
  };
}
module.exports = connect(select)(LaundryVroomLoader);
