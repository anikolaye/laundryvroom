/**
* @providesModule LaundryVroomMapMarker
* @flow
*/
import React, {
  Component,
  PropTypes
} from 'react';

import {
  ActivityIndicator,
  StyleSheet,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

const LaundryVroomColors = require('LaundryVroomColors');
import Icon from 'react-native-vector-icons/Entypo';

class LaundryVroomMapMarker extends Component {
  propTypes: {
    goToSelection: React.PropTypes.func,
  };

  props: {
    navigator: Navigator;
    fontSize: any;
    address:string;
    locationProcessing:false;
    goToSelection: () => void;
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bubble}>
          <LvText style={[styles.bubbleText, { fontSize: this.props.fontSize }]}>Clean My Laundry!</LvText>
          {this.props.locationProcessing ? (
            <View style={styles.loading}>
               <ActivityIndicator animating={true} color={LaundryVroomColors.backgroundColor} size="small"/>
            </View>
          ) : (
          <TouchableOpacity onPress={this.props.goToSelection}>
            <Icon style={styles.rightArrowButton} name="chevron-with-circle-right" size={23} color={LaundryVroomColors.backgroundColor}/>
           </TouchableOpacity>
         )}

        </View>
        <View style={styles.arrowBorder} />
        <View style={styles.arrow} />
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
  },
  bubble: {
    flex: 0,
    flexDirection: 'row',
    alignSelf: 'flex-start',
    backgroundColor: LaundryVroomColors.darkBackground,
    padding: 11,
    borderRadius: 20,
    borderColor: LaundryVroomColors.darkText,
    borderWidth: 0.5,
  },
  bubbleText: {
    color: '#FFFFFF',
    fontSize: 16,
  },
  arrow: {
    backgroundColor: 'transparent',
    borderWidth: 15,
    borderColor: 'transparent',
    borderTopColor: LaundryVroomColors.darkBackground,
    alignSelf: 'center',
    marginTop: -15,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderWidth: 9,
    borderColor: 'transparent',
    borderTopColor: LaundryVroomColors.darkBackground,
    alignSelf: 'center',
    marginTop: -5.5,
  },
  loading: {
    marginLeft:13,
    width: 25,
    height: 25
  },
  rightArrowButton: {
    marginLeft:13,
    width: 25,
    height: 25,
  },
});

module.exports = LaundryVroomMapMarker;
