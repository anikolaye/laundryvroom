/**
* @providesModule CustomerMap
* @flow
*/
'use strict';
import React, {
  Component
} from 'react';

import {
  ActivityIndicator,
  AppState,
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  Modal
} from 'react-native';

var { LvText } = require('LaundryVroomText');
const { connect } = require('react-redux');
import type {Dispatch} from '../actions/types';
import type {Customer} from '../reducers/customer';
import type {MerchantLocation} from '../reducers/merchantLocation';

var {
  findMerchants,
  resetOrder,
  resetOrderLines
}  = require('../../actions');

import MapView from 'react-native-maps';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import Geocoder from 'react-native-geocoder';
import Icon from 'react-native-vector-icons/FontAwesome';

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
const LaundryVroomMapMarker = require('LaundryVroomMapMarker');
// const Navigator = require('Navigator');

var screen = Dimensions.get('window');
const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.012;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.0002;

const homePlace = {description: 'Home', geometry: { location: { lat: 0, lng: 0 } }};

var { STORAGE_KEY, USER_ID, MAPS_API_KEY } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

class CustomerMap extends Component {
  props: {
    onLogin: () => void;
    customer: Customer;
    merchantLocation: MerchantLocation;
  };

  constructor(props) {
    super(props);
    var localRegion = props.region;
    if(!localRegion) {
      localRegion = {
        latitude:0,
        longitude:0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      };
    }
    this.state = {
      error:false,
      errorMessage:'',
      locationProcessing:false,
      region: localRegion,
      coordinate: {
        latitude: localRegion.latitude,
        longitude: localRegion.longitude + SPACE,
      },
      address: props.address,
      formattedAddress: props.formattedAddress,
      locationOn:true
    }
  }
  componentWillMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        navigate("Landing");
      }
    }.bind(this));
  }

  componentDidMount() {
    this.props.dispatch(resetOrder());
    this.props.dispatch(resetOrderLines());
    AppState.addEventListener('change', this.handleAppStateChange);
    if(this.props.customer
      && this.props.customer.shippingAddress
      && this.props.customer.shippingAddress.latitude
      && this.props.customer.shippingAddress.longitude) {
      this.setState({
        locationOn:true,
        region: {
          latitude: this.props.customer.shippingAddress.latitude,
          longitude: this.props.customer.shippingAddress.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }
      });
    } else {
      this.setState({locationOn:true});
      this._getCurrentLocation();
    }
  }
  handleAppStateChange = (appState) => {
    if (appState === 'active') {
      AppState.addEventListener('change', this.handleAppStateChange);
      if(this.props.customer
        && this.props.customer.shippingAddress
        && this.props.customer.shippingAddress.latitude
        && this.props.customer.shippingAddress.longitude) {
        this.setState({
          locationOn:true,
          region: {
            latitude: this.props.customer.shippingAddress.latitude,
            longitude: this.props.customer.shippingAddress.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      } else {
        this.setState({locationOn:true});
        // this._getCurrentLocation();
      }
    }
  }
  _getCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        var reversePosition = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        Geocoder.geocodePosition(reversePosition).then(origRes => {
          if(origRes) {
            var res = origRes[0];
            var address = {
              addressLine1: '',
              addressLine2: '',
              city: '',
              state: '',
              zip: '',
              latitude : position.coords.latitude,
              longitude: position.coords.longitude
            };
            if(res.streetName != null && res.streetNumber != null) {
              address.addressLine1 = res.streetNumber + ' ' + res.streetName;
            } else if(res.feature != null) {
              address.addressLine1 = res.feature;
            }
            if(res.postalCode) {
              address.zip = res.postalCode;
            }
            if(res.locality) {
              address.city = res.locality;
            }

            if(res.adminArea) {
              address.state = res.adminArea;
            }

            var formattedAddress = '';
            if(address) {
              formattedAddress = address.addressLine1 +
                                    ", " + address.city +
                                    ", " + address.state +
                                    " " + address.zip;
            }
            this.setState({address: address, formattedAddress:formattedAddress});
          }
        })
        .catch(err => console.log(err));
        this.setState({
          locationOn:true,
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA + SPACE,
            longitudeDelta: LONGITUDE_DELTA + SPACE,
          }
        });
      },
      (error) => {
        this.setState({locationOn:false});
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  render () {
    var modalBackgroundStyle = {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    };
    var innerContainerTransparentStyle = {
      backgroundColor: '#fff',
      padding: 20
    };
    var activeButtonStyle = {
      backgroundColor: '#ddd'
    };

    return (
          <View style={styles.grid}>
            <MapView
              ref="map"
              style={styles.map}
              initialRegion={this.state.region}
              region={this.state.region}
              zoomEnabled={false}
            >
            {this.state.locationOn ? (
              <MapView.Marker
                coordinate={
                  (this.state.coordinate.latitude ? this.state.coordinate : this.state.region )
                }
              >
                <LaundryVroomMapMarker
                  fontSize={17}
                  address={this.state.formattedAddress}
                  goToSelection={this._goToSelection.bind(this)}
                  locationProcessing={this.state.locationProcessing}
                />
              </MapView.Marker>
            ) : null
            }
          </MapView>
          <View style={styles.errorContainer}>
            <Modal
              animationType={"fade"}
              transparent={true}
              visible={this.state.error}
              onRequestClose={() => {this._setModal.bind(this,false)}}
              >
              <View style={[styles.container, modalBackgroundStyle]}>
                <View style={[styles.innerContainer, innerContainerTransparentStyle]}>
                  <LvText style={styles.error}>
                    {this.state.errorMessage}
                  </LvText>
                  <LaundryVroomButton
                     onPress={this._setModal.bind(this, false)}
                     style={[styles.button, this.props.style, styles.buttonBottom]}
                     icon={
                      ""
                     }
                     loading={false}
                     caption="Close"
                   />
                 </View>
              </View>
            </Modal>
          </View>
          {!this.state.locationOn ? (
            <View>
              <LvText>Oops! It appears we are unsure where you are!  Please enable location services in your settings to continue.</LvText>
            </View>
            ) : null
          }
          {this.state.formattedAddress ? (
            <GooglePlacesAutocomplete
              listViewDisplayed='auto'
              placeholder='Select Pickup Location'
              minLength={2} // minimum length of text to search
              autoFocus={false}
              fetchDetails={true}
              onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                var address = {};
                if(this.state.address) {
                  address = this.state.address;
                }
                this._setAddress(details);
                address.addressLine1 = details.name;
                var region = {};
                region.latitude = details.geometry.location.lat;
                region.longitude = details.geometry.location.lng;
                region.latitudeDelta =  LATITUDE_DELTA;
                region.longitudeDelta = LONGITUDE_DELTA;

                var coordinate = {
                  latitude: details.geometry.location.lat + SPACE,
                  longitude: details.geometry.location.lng,
                };

                this.setState({
                  region: region,
                  coordinate: coordinate,
                });
              }}
              enablePoweredByContainer={true}
              getDefaultValue={() => {
                return this.state.formattedAddress; // text input default value
              }}
              query={{
                // available options: https://developers.google.com/places/web-service/autocomplete
                key:'AIzaSyBCwKxy-rNiLT7XTYnWqM0sh7O-dgZujo4',
                language: 'en', // language of the results
              }}
              styles={{
                container: {
                  flex:0,
                  padding:20,
                },
                description: {
                  fontWeight: 'bold',
                  fontFamily:LaundryVroomColors.fontFamily
                },
                predefinedPlacesDescription: {
                  color: LaundryVroomColors.darkBackground,
                  fontFamily:LaundryVroomColors.fontFamily
                },
                textInput: {
                  fontFamily:LaundryVroomColors.fontFamily
                },
                textInputContainer: {
                  backgroundColor: '#FFFFFF',
                  height: 44,
                  borderTopColor: LaundryVroomColors.cellBorder,
                  borderBottomColor: LaundryVroomColors.cellBorder,
                  borderTopWidth: 0.5,
                  borderBottomWidth: 0.5
                },
                row: {
                  backgroundColor:'#FFF'
                }
              }}
              currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
              currentLocationLabel="Current location"
              nearbyPlacesAPI='GoogleReverseGeocoding' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
              GoogleReverseGeocodingQuery={{
                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
              }}
              GooglePlacesSearchQuery={{
                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                rankby: 'distance',
                types: 'food',
              }}
              filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
            />
          ) : null
        }
      </View>
    );
  }

  _setModal(display) {
    this.setState({error:display});
  }

  _setAddress(address) {
    var newAddress = {};
    if(this.state.address) {
      newAddress =  this.state.address;
    }

    newAddress.latitude = address.geometry.location.lat;
    newAddress.longitude = address.geometry.location.lng;
    var street_number = '';
    var route = '';
    for(var i = 0; i < address.address_components.length; i++) {
      for(var j = 0; j< address.address_components[i].types.length; j++) {
        switch(address.address_components[i].types[j]) {
          case 'street_number':
            street_number = address.address_components[i].long_name;
            break;
          case 'route':
            route = address.address_components[i].long_name;
            break;
          case 'locality':
            newAddress.city = address.address_components[i].long_name;
            break;
          case 'administrative_area_level_1':
            newAddress.state = address.address_components[i].short_name;
            break;
          case 'country':
            newAddress.country = address.address_components[i].short_name;
            break;
          case 'postal_code':
            newAddress.zip = address.address_components[i].long_name;
            break;
        }
      }
    }
    newAddress.addressLine1 = street_number + ' ' + route;

    this.setState({address:newAddress});
  }

  _goToSelection() {
    let {navigate} = this.props.navigation;
    this.setState({locationProcessing:true, error:false, errorMessage:''});
    this.props.dispatch(findMerchants(this.state.address.latitude, this.state.address.longitude)).then(function() {
      if(this.props.merchantLocation && this.props.merchantLocation.id) {
        var order = this.props.order || [];
        order['address'] = this.state.address;
        order['merchantLocation'] = this.props.merchantLocation;
        this.setState({locationProcessing:false, error:false, errorMessage:''});
        navigate("ProductSelection", {order:order});
      } else {
        if(this.props.merchantLocation && this.props.merchantLocation.error) {
          this.setState({locationProcessing:false, error:true, errorMessage:this.props.merchantLocation.errorMessage});
        } else {
          this.setState({locationProcessing:false, error:true, errorMessage:'An unknown error occurred, please try again.'});
        }
      }
    }.bind(this));
  }
}

var styles = StyleSheet.create({
  grid: {
    backgroundColor:'white',
    flex:1,
  },
  map: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    position:'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  locationInput: {
    height: 40,
    padding:20,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  innerContainer: {
    borderRadius: 10,
    alignItems: 'center',
  },
  error: {
    fontWeight:'bold',
    fontSize:14,
    marginBottom:15
  }
});

function select(store) {
  return {
    order:store.order,
    orderLines:store.orderLines,
    customer: store.customer,
    merchantLocation:store.merchantLocation
  };
}

module.exports = connect(select)(CustomerMap);
