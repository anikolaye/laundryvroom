/**
* @providesModule Privacy
* @flow
*/
import React, {
  Component
} from 'react';

import {
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  Modal
} from 'react-native';

//Drawer icon
import Icon from 'react-native-vector-icons/FontAwesome';

var { LvText } = require('LaundryVroomText');

const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
// const Navigator = require('Navigator');
var { USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

class Privacy extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Privacy Policy',
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.navigate('DrawerOpen')}
          style={{"marginLeft":10}}
        >
          <Icon name="bars" size={30} color="#fff" />
       </TouchableOpacity>
     ),
     headerRight:(
       <TouchableOpacity
          onPress={() => navigation.navigate('Drawer')}
          style={{"marginRight":10}}
        >
          <Icon name="map" size={30} color="#fff" />
       </TouchableOpacity>
     )
  });

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        navigate("Landing");
      }
    }.bind(this));
  }

  render () {
    var LaundryVroomEmail = "legal@laundryvroom.com";
     return (
       <View style={{flex:1}}>
         <ScrollView
            style={styles.termsScroll}
         >
          <View style={styles.grid}>
            <LvText style={styles.gridItem,styles.extraMargin}>
              This privacy policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information' (PII) is being used online. PII, as used in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              What personal information do we collect from the people that visit our blog, website or app?
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, phone number or other details to help you with your experience.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              When do we collect information?
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We collect information from you when you register on our site or enter information on our site.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              How do we use your information?
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              • To quickly process your transactions.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              How do we protect visitor information?
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We use regular Malware Scanning.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              All transactions are processed through a gateway provider and are not stored or processed on our servers.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              Do we use 'cookies'?
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We use cookies to:
                  • Understand and save user's preferences for future visits.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser's Help menu to learn the correct way to modify your cookies.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              If you disable cookies off, some features will be disabled It won't affect the user's experience that make your site experience more efficient and some of our services will not function properly.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              However, you can still place orders .
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              Third-party disclosure
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property, or safety.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              Third-party links
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We do not include or offer third-party products or services on our website.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              Google
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We use Google AdSense Advertising on our website.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We have implemented the following:
                  • Demographics and Interests Reporting
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Opting out:
              Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising initiative opt out page or permanently using the Google Analytics Opt Out Browser add on.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              California Online Privacy Protection Act
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require a person or company in the United States (and conceivably the world) that operates websites collecting personally identifiable information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals with whom it is being shared, and to comply with this policy. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf
              </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              According to CalOPPA we agree to the following:
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Users can visit our site anonymously.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Once this privacy policy is created, we will add a link to it on our home page or as a minimum on the first significant page after entering our website.
              Our Privacy Policy link includes the word 'Privacy' and can be easily be found on the page specified above.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Users will be notified of any privacy policy changes:
                    • On our Privacy Policy Page
              Users are able to change their personal information:
                    • By logging in to their account
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              How does our site handle do not track signals?
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We honor do not track signals and do not track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              Does our site allow third-party behavioral tracking?
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
            It's also important to note that we do not allow third-party behavioral tracking
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              COPPA (Children Online Privacy Protection Act)
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              When it comes to the collection of personal information from children under 13, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, the nation's consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We do not specifically market to children under 13.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              Fair Information Practices
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We will notify the users via email
                    • Within 1 business day
              We will notify the users via in-site notification
                    • Within 1 business day
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              We also agree to the Individual Redress Principle, which requires that individuals have a right to pursue legally enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              CAN SPAM Act
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              We collect your email address in order to:
              </LvText>
              <LvText style={styles.gridItem, styles.gridData}>
                  • Send information, respond to inquiries, and/or other requests or questions.
                  • Process orders and to send information and updates pertaining to orders.
                  • Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              To be in accordance with CANSPAM we agree to the following:
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
                  • NOT use false or misleading subjects or email addresses.
                  • Identify the message as an advertisement in some reasonable way.
                  • Include the physical address of our business or site headquarters.
                  • Monitor third-party email marketing services for compliance, if one is used.
                  • Honor opt-out/unsubscribe requests quickly.
                  • Allow users to unsubscribe by using the link at the bottom of each email.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              If at any time you would like to unsubscribe from receiving future emails, you can email us at `${LaundryVroomEmail}`
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
                  • Follow the instructions at the bottom of each email.
            and we will promptly remove you from ALL correspondence.
            </LvText>
            <LvText style={styles.gridItem, styles.gridTitle}>
              Contacting Us
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              If there are any questions regarding this privacy policy you may contact us using the information below.
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              laundryvroom.com
              14055 Tahiti Way
              Marina Del Rey, CA 90292
              USA
              anikolaye@gmail.com
            </LvText>
            <LvText style={styles.gridItem, styles.gridData}>
              Last Edited on 2016-04-25
            </LvText>
          </View>
         </ScrollView>
       </View>
     );
   }

}

var styles = StyleSheet.create({
  termsScroll: {
    flex:1
  },
  grid: {
    flex:1,
    flexDirection:'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginLeft:10
  },
  gridRow: {
    flexDirection:'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  gridItem: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginLeft:10,
    marginBottom:5
  },
  gridData: {
    fontSize:11
  },
  gridTitle: {
    fontWeight:'bold',
    fontSize:12
  },
  extraMargin: {
    marginTop:10
  }
});

module.exports = Privacy;
