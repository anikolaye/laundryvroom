/**
* @providesModule Terms
* @flow
*/
import React, {
  Component
} from 'react';

import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Modal
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

var { LvText } = require('LaundryVroomText');

const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
// const Navigator = require('Navigator');
var { USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

class Terms extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Terms & Conditions',
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.navigate('DrawerOpen')}
          style={{"marginLeft":10}}
        >
          <Icon name="bars" size={30} color="#fff" />
       </TouchableOpacity>
     ),
     headerRight:(
       <TouchableOpacity
          onPress={() => navigation.navigate('Drawer')}
          style={{"marginRight":10}}
        >
          <Icon name="map" size={30} color="#fff" />
       </TouchableOpacity>
     )
  });
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        navigate("Landing");
      }
    }.bind(this));
  }

  render () {
     var laundryVroomPhone = 'xxx-xxx-xxxx';
     return (
       <View style={{flex:1}}>
         <ScrollView
           style={styles.termsScroll}
         >
          <View style={styles.grid}>
            <LvText style={styles.gridItem}>
              The legal (boring) stuff... Queue the guy that speaks really fast on infomercials...
            </LvText>
            <LvText style={styles.gridItem}>
              You have clothes that need to get cleaned and it is our goal at LaundryVroom LLC to get the job done with the utmost care, quality, and expedience. We use the most modern equipment available and give each one of your garments the individual care it deserves.
            </LvText>
            <LvText style={styles.gridItem}>
              By signing up with LaundryVroom LLC and opening your online account, you agree to the following terms and conditions; These terms and conditions are subject to change and though we try to keep our customers as up to date as much as possible with any changes, it is the responsibility of you, the customer, to review these terms prior purchase of any services.
            </LvText>
            <LvText style={styles.gridItem}>
              If you are not 100% pleased with our services, if any of your items are missing, or to report any damage to your garments, you must contact our customer service number (`${laundryVroomPhone}`) within 24 hours of your scheduled delivery date and time. After this period, no refunds will be given or claims responded to.
            </LvText>
            <LvText style={styles.gridItem}>
              Our wash / dry / fold service is for a minimum of 15 lbs and is set up in per lb rates of $2.50, plus a processing fee of $2.00 per order. The bags will be weighed at our facility and we reserve the right to automatically bill the credit or debit card you have provided for your purchase LaundryVroom LLC will not honor any weighing disputes and we do not give any refunds for claims involving load weight.
            </LvText>
            <LvText style={styles.gridItem}>
              At LaundryVroom LLC, we individually inspect every item at the facility before and after they are processed and do our best to find any items that may have been forgotten by a customer in pockets or other areas of the laundry. However  LaundryVroom LLC is not responsible for any items left in pockets such as pens or other items which may harm laundry during the washing or drying processes. Additionally, any items which are purposely tore or damaged such as jeans, denim garments, etc… by the manufacturer of the garment or the customer can never be included in any damage claim for any reason.
            </LvText>
            <LvText style={styles.gridItem}>
              All of our wash/dry/fold cycles use cold washes. This means that we will never honor any claims of shrinking, color fading, or color blending. These are all normal occurrences to some degree in all laundry processes and will never be considered a form of damage by LaundryVroom LLC. Additionally, some stains cannot be removed in a cold wash. We do no promise to remove any stain but make sure to spot treat any stain with a removing-pen and or liquid substance and double check the stain before the drying process.
            </LvText>
            <LvText style={styles.gridItem}>
              There are some stains that have been scientifically proven not to appear until exposed to water or heat. Such stains typically have a sugar base. Some examples of stains are alcohol and juices. Such stains can be proved in a lab, but the expense is $200 to test. If it turns out that LaundryVroom LLC is responsible for the discoloration, then LaundryVroom LLC will assume the lab charge and suitable reparation for the garment. If LaundryVroom LLC is not responsible for the discoloration, i.e., it is a sugar based stain that was invisible before exposure to water/heat, the customer is responsible for the lab charge and will not be reimbursed for the garment. No garment will ever be sent to a lab without customer’s request. LaundryVroom LLC does not use sugar based stains as an escape clause for damaged garments since it is easy for an expert to identify the cause of a stain.
            </LvText>
            <LvText style={styles.gridItem}>
              By putting any item into your laundry or dry cleaning bags, you are guaranteeing ownership and responsibility of that item. We highly recommend not including items that do not belong to you such as your roommates’ or friends’ to avoid any conflict. If they really need a specific item to be washed, have them set up a separate account separate their belongings.
            </LvText>
            <LvText style={styles.gridItem}>
              If you wish to report a missing or stolen item, you must be able to provide a specific description of the item such as the type of garment, brand, color, size… While we will investigate any claim of stolen or missing items to the best of our ability, we do not offer any compensation for missing or stolen garments. Because we take such care in individually handling and sorting garments, we find that the vast majority of claims are in fact the fault of the customer misplacing or simply not including a garment that they thought was in their laundry bag. We recommend including a detailed list of all items in the bag which will be checked upon reaching our facility if this is a concern. This list will not be considered a formal form of inventory, but it will help us to identify the items you include.
             </LvText>
            <LvText style={styles.gridItem}>
              At LaundryVroom LLC, we make sure to individually handle and check all items for damages, contents, and to count each item. This happens before and after the wash/dry/fold and dry cleaning process. We accept no financial responsibility for any items left in the customers garments like cash, jewelry, credit cards, etc… We practice extremely thorough pocket checks for any items such as ink pens, markers, highlighters, makeup, or other items which may cause serious damage to your garments, but accept no responsibility for damages that are caused by these items. The best way to ensure that these damages never occur is to do what we do before you send it in. Check each individual item before putting it in the laundry / dry cleaning bag, even if it doesn’t have pockets. And also double check to make sure nothing other then your garments found its way into your laundry / dry cleaning bag.
             </LvText>
            <LvText style={styles.gridItem}>
              While we try as hard as we can to make our service as convenient for you as possible, we do not accept responsibility for items lost or stolen if they are left for pickup at a pre-designated area rather then a hand to hand exchange from customer to driver.
             </LvText>
            <LvText style={styles.gridItem}>
              Wash Dry Fold Policy - LaundryVroom LLC will not cover any reimbursement over $200 or any individual item over $20. LaundryVroom LLC reserves the right to decide if any reimbursement will be in cash or credit (to our services) form. We are also not responsible for any fire, theft, flooding, or other occurrence out of the control of LaundryVroom LLC and it's employees.
             </LvText>
            <LvText style={styles.gridItem}>
              Dry Cleaning / Tailoring Policy- LaundryVroom LLC exercises the utmost care in processing articles entrusted to us and use such processes which, in our opinion are the best suited to the nature and condition of each individual article. Nevertheless we cannot assume responsibility for inherent weaknesses of or defects in materials that are not readily apparent prior to processing. This applies particularly, but not exclusively, to suedes, leathers, silks, satins, double face fabrics, vinyls, polyurethanes, etc... Responsibility also is disclaimed for trimmings, buckles, belts, beads, buttons, and sequins. In laundering we cannot guarantee against color loss and shrinkage; or against damage to weak and tender fabrics. Differences in count must be reported within 24 hours. Unless a list accompanied the bundle our count must be accepted. The company's liability with respect to any lost article shall not exceed 10 times our charge for processing it.
             </LvText>
            <LvText style={styles.gridItem}>
              LaundryVroom LLC reserves the right to change these terms at any time and refuse service to any customer should conflict arise. All of our operating information such as prices and operating hours can be found on our website. We will never take a order by phone and if any necessary information is missing for a customers account, we reserve the right to refuse service.
             </LvText>
            <LvText style={styles.gridItem}>
              LaundryVroom LLC also reserves the right to cancel any order and issue a full refund of any money spent on said order if there is a pricing error or website glitch which led to improper payment amounts received. LaundryVroom LLC also is not responsible for any monthly services started by a user. It is the user's sole responsibility to start and stop these services and any failure to do so will not result in a refund from LaundryVroom LLC. The user is responsible for stopping monthly services which can easily be accessed under the "My Plans" tab when logged in.
             </LvText>
            <LvText style={styles.gridItem}>
              Due to the unpredictability of weather conditions or other natural occurrences, LaundryVroom LLC reserves the right to make any pickup or delivery cancellations for the safety of the drivers. Contact will be made with the customer as soon as possible so that a future pickup/delivery may be made. All attempts will made to honor the original pickup and delivery times before resorting to a schedule change.
             </LvText>
            <LvText style={styles.gridItem}>
              Did you just read that whole thing? Impressive lol... That all being said bottom line we will take great care of your stuff and provide you with a high level of customer service... Just set up the order! :-)
            </LvText>
           </View>
         </ScrollView>
       </View>
     );
   }

   _goBack() {
     this.props.navigator.pop();
   }
}

var styles = StyleSheet.create({
  termsScroll: {
    flex:1
  },
  grid: {
    flex:1,
    flexDirection:'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin:0
  },
  gridRow: {
    flexDirection:'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  gridItem: {
    fontSize:11,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginLeft:10,
    marginBottom:5
  },
  extraMargin: {
    marginTop:10
  }
});

module.exports = Terms;
