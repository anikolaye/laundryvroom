/**
*
* @providesModule CustomerMenu
* @flow
*/

import React, {
  Component,
  PropTypes
} from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

const { connect } = require('react-redux');
import type {Customer} from '../reducers/customer';
import type {Dispatch} from '../actions/types';

type Props = {
  customer: Customer;
  navigator: any;
  dispatch: Dispatch;
};

const LaundryVroomColors = require('LaundryVroomColors');
// const Navigator = require('Navigator');

class CustomerMenu extends Component {
  static propTypes = {
    closeDrawer: PropTypes.func.isRequired,
  };

  props: Props;
  constructor(props : Props) {
    super(props);
  }

  _linkTo(link) {
    this.props.closeDrawer();
    this.props.navigator.push({[link]:true});
  }

  render() {
    let {customer} = this.props;
    let {closeDrawer} = this.props;
    return (
      <View style={styles.menuDrawer}>
        <View style={styles.menuHeader}>
            <LvText style={[styles.menuText, styles.userName]}>
              {customer.firstName} {customer.lastName}
          </LvText>
        </View>
        <View style={styles.menuItems}>
          <TouchableOpacity style={styles.menuItem} onPress={this._linkTo.bind(this, 'account')}>
            <LvText style={styles.menuText}>My Account</LvText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.menuItem} onPress={this._linkTo.bind(this,'orderHistory')}>
            <LvText style={styles.menuText}>Orders</LvText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.menuItem} onPress={this._linkTo.bind(this,'pricing')}>
            <LvText style={styles.menuText}>Pricing</LvText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.menuItem} onPress={this._linkTo.bind(this,'terms')}>
            <LvText style={styles.menuText}>Terms & Conditions</LvText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.menuItem} onPress={this._linkTo.bind(this,'privacy')}>
            <LvText style={styles.menuText}>Privacy Policy</LvText>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  menuDrawer: {
    flex:1,
    backgroundColor: LaundryVroomColors.darkBackground,
    borderRightColor: LaundryVroomColors.lightText,
    borderRightWidth:1,
    paddingTop:40
  },
  menuHeader: {
    flexDirection:'row',
    // paddingTop:10,
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    paddingBottom:10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuItems: {
    paddingLeft:10,
    paddingTop:5,
  },
  menuItem: {
    paddingTop:20,
    paddingBottom:10
  },
  toolbar: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  menuText: {
    color:'white',
    fontWeight:'bold',
    fontSize:16,
  },
  closeButton: {
    height:25,
    width:25
  }
});

function select(store) {
  return {
    customer: store.customer,
  };
}

module.exports = connect(select)(CustomerMenu);
