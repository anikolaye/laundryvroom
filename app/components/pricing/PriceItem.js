/**
* @providesModule PriceItem
* @flow
*/
import React, {
  Component
} from 'react';

import {
  ListView,
  StyleSheet,
  View
} from 'react-native';

var { LvText } = require('LaundryVroomText');

type Props = {
  tabLabel:any;
  sku: any;
  product: any;
};

const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomColors = require('LaundryVroomColors');

var ScrollableTabView = require('react-native-scrollable-tab-view');

class PriceItem extends Component {
  props: Props;

  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      categories: [],
    };
  }

  componentDidMount() {
    let {product} = this.props
    if(product) {
      var womenCategory = [];
      var menCategory = [];
      var otherCategory = [];
      var categories = [];
      product.productFees.forEach(function(productFee) {
        switch(productFee.category) {
          case 'MEN':
            menCategory.push(productFee);
            break;
          case 'WOMEN':
            womenCategory.push(productFee);
            break;
          case 'OTHER':
            otherCategory.push(productFee);
            break;
        }
      });
      if(menCategory.length > 0) {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        categories.push({name: 'Men', fees: ds.cloneWithRows(menCategory)});
      }
      if(womenCategory.length > 0) {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        categories.push({name: 'Women', fees: ds.cloneWithRows(womenCategory)});
      }
      if(otherCategory.length > 0) {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        categories.push({name: 'Other', fees: ds.cloneWithRows(otherCategory)});
      }
      this.setState({categories: categories});
    }
  }
  render () {
    return(
      <View ref={this.props.sku} style={{flex:1}}>
        <View style={{padding:10}}>
         <LvText style={{fontStyle:"italic", fontSize:15}}>Minimum Order Charge: $30.00</LvText>
         <LvText style={{fontStyle:"italic", fontSize:15}}>Cancelation Fee: $5.00</LvText>
        </View>
        <ListView
          ref={this.props.sku}
          style={styles.ordersContainer}
          dataSource={this.props.fees}
          renderRow={this._renderRow.bind(this)}
          renderSeparator={this._renderSeparator}
          contentInset={{bottom:49}}
          enableEmptySections={true}
        />
      </View>
    );
   }

   _renderRow(product) {
      if(product.category == "OTHER"){
        return(
          <View style={styles.row}>
           <LvText><LvText style={styles.name}>{product.name}</LvText> - ${product.baseFee.toFixed(2)}</LvText>
            {product.description != product.name ? (
              <LvText style={styles.description}>{product.description}</LvText>
            ): null}
          </View>
        );
     } else {
       return(
         <View style={styles.row}>
           <LvText><LvText style={styles.name}>{product.name}</LvText> - ${product.baseFee.toFixed(2)}</LvText>
        </View>
       );
     }
   }

   _renderSeparator(sectionID: number, rowID: number, adjacentRowHighlighted: bool) {
    return (
      <View
        key={`${sectionID}-${rowID}`}
        style={{
          height: adjacentRowHighlighted ? 4 : 1,
          backgroundColor: adjacentRowHighlighted ? '#3B5998' : '#CCCCCC',
        }}
      />
    );
  }
}

var styles = StyleSheet.create({
  row: {
    padding:10
  },
  name: {
    fontWeight:'bold'
  },
  description: {
    paddingTop:10
  }
});

module.exports = PriceItem;
