/**
* @providesModule PriceCategory
* @flow
*/
import React, {
  Component
} from 'react';

import {
  ListView,
  StyleSheet,
  View
} from 'react-native';

type Props = {
  tabLabel:any;
  sku: any;
  product: any;
};

const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomColors = require('LaundryVroomColors');

var ScrollableTabView = require('react-native-scrollable-tab-view');
var PriceItem = require('PriceItem');
class PriceCategory extends Component {
  props: Props;

  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      categories: [],
    };
  }

  componentDidMount() {
    let {product} = this.props
    if(product) {
      var womenCategory = [];
      var menCategory = [];
      var otherCategory = [];
      var categories = [];
      product.productFees.forEach(function(productFee) {
        switch(productFee.category) {
          case 'MEN':
            menCategory.push(productFee);
            break;
          case 'WOMEN':
            womenCategory.push(productFee);
            break;
          case 'OTHER':
            otherCategory.push(productFee);
            break;
        }
      });
      if(menCategory.length > 0) {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        categories.push({name: 'Men', fees: ds.cloneWithRows(menCategory)});
      }
      if(womenCategory.length > 0) {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        categories.push({name: 'Women', fees: ds.cloneWithRows(womenCategory)});
      }
      if(otherCategory.length > 0) {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        if(product.sku == 'WASH') {
          categories.push({name: 'Machine Wash', fees: ds.cloneWithRows(otherCategory)});
        } else {
          categories.push({name: 'Other', fees: ds.cloneWithRows(otherCategory)});
        }

      }
      this.setState({categories: categories});
    }
  }
  render () {
    let { categories } = this.state;
    var rows = [];
    categories.forEach(function(category) {
      rows.push(this._renderCategory(category.fees,category.name, this.props.product.sku));
    }.bind(this));
    return(
      <View ref={this.props.product.sku} style={{flex:1}}>
        <ScrollableTabView
          ref={this.props.product.sku}
          tabBarTextStyle={{fontSize: 15, fontFamily:LaundryVroomColors.fontFamily, color:LaundryVroomColors.darkBackground}}
          tabBarUnderlineStyle={{backgroundColor:LaundryVroomColors.darkBackground}}
          >
          {rows}
        </ScrollableTabView>
      </View>
    );
   }

   _renderCategory(fees, name, sku) {
     return (
         <PriceItem key={name} tabLabel={name} fees={fees} sku={sku}/>
     );
   }
}

var styles = StyleSheet.create({
  left: {
    paddingLeft:10
  }
});

module.exports = PriceCategory;
