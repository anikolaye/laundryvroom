/**
* @providesModule Pricing
* @flow
*/
import React, {
  Component
} from 'react';

import {
  ActivityIndicator,
  ListView,
  Modal,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View
} from 'react-native';

var { LvText } = require('LaundryVroomText');

const { connect } = require('react-redux');

import type {Dispatch} from '../actions/types';
import type {Products} from '../reducers/products';
import type {Customer} from '../reducers/customer';

type Props = {
  customer: any;
  products: any;
  navigator: any;
  dispatch: Dispatch;
};

var {
  findProducts
} = require('../../actions');

import Icon from 'react-native-vector-icons/FontAwesome';

const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
const LaundryVroomColors = require('LaundryVroomColors');
// const Navigator = require('Navigator');
var { USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

var ScrollableTabView = require('react-native-scrollable-tab-view');
var PriceCategory = require('PriceCategory');

class Pricing extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Pricing',
     headerLeft:((navigation.state.params && navigation.state.params.referrer == "Summary") ?
       <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{"marginLeft":10}}
        >
          <Icon name="chevron-left" size={30} color="#fff" />
       </TouchableOpacity>
     :
       <TouchableOpacity
          onPress={() => navigation.navigate('DrawerOpen')}
          style={{"marginLeft":10}}
        >
          <Icon name="bars" size={30} color="#fff" />
       </TouchableOpacity>
     ),
     headerRight:((navigation.state.params && navigation.state.params.referrer == "Summary") ? null :
       <TouchableOpacity
          onPress={() => navigation.navigate('Drawer')}
          style={{"marginRight":10}}
        >
          <Icon name="map" size={30} color="#fff" />
       </TouchableOpacity>
     )
  });
  props: Props;

  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      washProduct: null,
      dryCleanProduct: null,
      loading:true
    };
  }

  componentDidMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        navigate("Landing");
      }
    }.bind(this));
    if(!this.props.products || this.props.products.length < 1) {
      this.props.dispatch(findProducts()).then(function() {
        if(this.props.products) {
          this.props.products.forEach(function(product) {
            if(product.sku == "WASH") {
              this.setState({washProduct: product});
            } else if(product.sku == "DRYCLEAN") {
              this.setState({dryCleanProduct: product});
            }
          }.bind(this));
          this.setState({loading:false});
        }
      }.bind(this));
    } else {
      this.props.products.forEach(function(product) {
        if(product.sku == "WASH") {
          this.setState({washProduct: product});
        } else if(product.sku == "DRYCLEAN") {
          this.setState({dryCleanProduct: product});
        }
      }.bind(this));
      this.setState({loading:false});
    }
  }

  render () {
    return (
     <View style={{flex:1}}>
       {this.state.loading  ? (
         <View style={styles.grid}>
            <ActivityIndicator animating={true} color={LaundryVroomColors.darkBackground} size="large"/>
            <LvText>Loading...</LvText>
         </View>
       ) : (
         <View style={styles.pricingScroll}>
          <ScrollableTabView
            tabBarTextStyle={{fontSize: 15, fontFamily:LaundryVroomColors.fontFamily, color:LaundryVroomColors.darkBackground}}
            tabBarUnderlineStyle={{backgroundColor:LaundryVroomColors.darkBackground}}
          >
            <PriceCategory tabLabel="Wash & Dry" sku="WASH" product={this.state.washProduct} />
            <PriceCategory tabLabel="Dry Clean" sku="DRYCLEAN" product={this.state.dryCleanProduct} />
           </ScrollableTabView>
        </View>
       )}
       </View>
    );
   }

   _renderRow(product) {
     if(product.category == "WOMEN") {
        return(
          <View style={styles.left}>
            <LvText style={styles.name}>{product.name} - ${product.baseFee.toFixed(2)}</LvText>
         </View>
        );
      } else if(product.category == "OTHER"){
        return(
          <View style={styles.left}>
            <LvText style={styles.name}>{product.name} - ${product.baseFee.toFixed(2)}</LvText>
            {product.description != product.name ? (
              <LvText style={styles.description}>{product.description}</LvText>
            ): null}
          </View>
        );
     } else {
       return (null);
     }
   }

   _renderSeparator(sectionID: number, rowID: number, adjacentRowHighlighted: bool) {
    // return (
    //   <View
    //     key={`${sectionID}-${rowID}`}
    //     style={{
    //       height: adjacentRowHighlighted ? 4 : 1,
    //       backgroundColor: adjacentRowHighlighted ? '#3B5998' : '#CCCCCC',
    //     }}
    //   />
    // );
    return null;
  }
}

var styles = StyleSheet.create({
  grid: {
    flex:1,
    flexDirection:'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin:0
  },
  pricingScroll: {
    flex:1,
    flexDirection:'column'
  },
  tabs: {
    flexDirection:'row',
    justifyContent: 'space-between',
    borderBottomWidth:1,
    borderBottomColor:LaundryVroomColors.darkBackground,
    marginBottom:10
  },
  tabLeft: {
    flexDirection:'row',
    alignItems: 'center',
    alignSelf:'flex-start',
    marginRight:20
  },
  tabRight: {
    flexDirection:'row',
    alignItems: 'center',
    alignSelf:'flex-start',
    marginLeft:20
  },
  left: {
    paddingLeft:10
  },
  gridRow: {
    flex:1,
    alignSelf:'stretch',
    justifyContent: 'space-between',
  },
  gridItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function select(store) {
  return {
    products: store.products
  };
}

module.exports = connect(select)(Pricing);
