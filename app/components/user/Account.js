/**
* @providesModule Account
* @flow
*/
import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Alert
} from 'react-native';

import { NavigationActions } from 'react-navigation';
var { LvText } = require('LaundryVroomText');

const { connect } = require('react-redux');
const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
// const Navigator = require('Navigator');
var { STORAGE_KEY,USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

import { SegmentedControls } from 'react-native-radio-buttons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { Fumi } from 'react-native-textinput-effects';

import type {Customer} from '../reducers/customer';
import type {CustomerPreferences} from '../reducers/customerPreferences';
import type {Dispatch} from '../actions/types';

const dismissKeyboard = require('dismissKeyboard');

type Props = {
  customer: Customer;
  customerPreferences: CustomerPreferences;
  dispatch: Dispatch;
};

var {
  loadCustomer,
  loadCustomerPreferences,
  updateCustomer,
  updateCustomerPreferences
} = require('../../actions');

const detergentOptions = [
  {
    label:"Scented",
    value:"SCENTED",
  },
  {
    label:"Unscented",
    value:"UNSCENTED",
  },
];

const dressShirtOptions = [
  {
    label:"Dry Cleaned",
    value:"DRYCLEAN",
  },
  {
    label:"Washed",
    value:"LAUNDERED",
  },
  {
    label:"No Preference",
    value:"MERCHANT",
  }
];

const doorOptions = [
  {
    label:"Left at the door",
    value:true,
  },
  {
    label:"Delivered When I'm Home",
    value:false,
  },
];

class Account extends Component {

  static navigationOptions = ({navigation}) => ({
     title: 'Account',
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.navigate('DrawerOpen')}
          style={{"marginLeft":10}}
        >
          <FontAwesomeIcon name="bars" size={30} color="#fff" />
       </TouchableOpacity>
     ),
     headerRight:(
       <TouchableOpacity
          onPress={() => navigation.navigate('Drawer')}
          style={{"marginRight":10}}
        >
          <FontAwesomeIcon name="map" size={30} color="#fff" />
       </TouchableOpacity>
     )
  });

  props: Props;
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone:'',
      selectedDoorOption:null,
      selectedDressShirtOption:null,
      selectedDetergentOption: null,
      saveProcessing:false,
      error:false,
      errorMessage:'',
      formValid:false
    }
  }

  componentDidMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        this._resetToLanding();
      } else {
        this.props.dispatch(loadCustomer(customerId)).then(function() {
          if(this.props.customer) {
            this.setState({
              firstName: this.props.customer.firstName,
              lastName: this.props.customer.lastName,
              phone: this.props.customer.phone,
              email: this.props.customer.email
            });
          }
          this.props.dispatch(loadCustomerPreferences(this.props.customer.id));
        }.bind(this));
      }
    }.bind(this));

    this._setPreferences();
  }

  render () {
    let {customerPreferences} = this.props;
    var dressShirtText = "Men's Dress shirts should be:";
    const { firstName,lastName,email,phone} = this.state;
    const firstNameValid = (firstName && firstName.length > 0 ? true : false);
    const lastNameValid = (lastName && lastName.length > 0 ? true : false);
    const emailValid = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);
    const phoneValid = true;
    const formValid = firstNameValid && lastNameValid && emailValid && phoneValid;
    return (
     <View style={styles.form}>
          <ScrollView keyboardShouldPersistTaps="always"
            contentInset={{bottom:100}}
            automaticallyAdjustContentInsets={false}>
            {this.state.error ? (
              <TouchableWithoutFeedback onPress={()=> dismissKeyboard()}>
                <View style={styles.errorContainer}>
                  <LvText style={styles.error}>
                    {this.state.errorMessage}
                  </LvText>
                </View>
              </TouchableWithoutFeedback>
            ) : null }
            <View>
              <Fumi
                ref={'FirstInput'}
                returnKeyType = {"next"}
                onSubmitEditing={(event) => {
                  this.refs.SecondInput._focus();
                }}
                blurOnSubmit={false}
                label={'First Name'}
                value={firstName}
                iconClass={MaterialIcon}
                iconName={'account-circle'}
                iconColor={LaundryVroomColors.darkBackground}
                // this is used as backgroundColor of icon container view.
                iconBackgroundColor={'#f2a59d'}
                inputStyle={{ color: '#464949' }}
                autoCorrect={false}
                autoCapitalize='sentences'
                autoCorrect={false}
                onChangeText={(text) => { this.setState({firstName: text}) }}
              />
              <Fumi
                ref={'SecondInput'}
                returnKeyType = {"next"}
                onSubmitEditing={(event) => {
                  this.refs.ThirdInput._focus();
                }}
                blurOnSubmit={false}
                label={'Last Name'}
                value={lastName}
                iconClass={MaterialIcon}
                iconName={'account-circle'}
                iconColor={LaundryVroomColors.darkBackground}
                // this is used as backgroundColor of icon container view.
                iconBackgroundColor={'#f2a59d'}
                inputStyle={{ color: '#464949' }}
                autoCorrect={false}
                autoCapitalize='sentences'
                autoCorrect={false}
                onChangeText={(text) => { this.setState({lastName: text}) }}
              />
              <Fumi
                ref={'ThirdInput'}
                returnKeyType = {"next"}
                onSubmitEditing={(event) => {
                  this.refs.FourthInput._focus();
                }}
                blurOnSubmit={false}
                label={'Phone'}
                iconClass={MaterialIcon}
                iconName={'phone'}
                value={phone}
                iconColor={LaundryVroomColors.darkBackground}
                // this is used as backgroundColor of icon container view.
                iconBackgroundColor={'#f2a59d'}
                inputStyle={{ color: '#464949' }}
                keyboardType='numeric'
                autoCorrect={false}
                onChangeText={(text) => { this.setState({phone: text}) }}
              />
              <Fumi
                ref={'FourthInput'}
                returnKeyType = {"done"}
                blurOnSubmit={false}
                label={'Email'}
                value={email}
                iconClass={FontAwesomeIcon}
                iconName={'envelope'}
                iconColor={LaundryVroomColors.darkBackground}
                // this is used as backgroundColor of icon container view.
                iconBackgroundColor={'#f2a59d'}
                inputStyle={{ color: '#464949' }}
                autoCapitalize='none'
                keyboardType='email-address'
                autoCorrect={false}
                onChangeText={(text) => { this.setState({email: text}) }}
              />
            </View>
            <TouchableWithoutFeedback onPress={()=> dismissKeyboard()}>
              <View>
                <View style={styles.extraMargin}>
                  <View style={[styles.inputWrap]}>
                    <LvText style={styles.label}>Detergents should be:</LvText>
                    <SegmentedControls
                      tint={'black'}
                      selectedBackgroundColor={LaundryVroomColors.darkBackground}
                      separatorTint={LaundryVroomColors.lightText}
                      containerBorderTint={LaundryVroomColors.lightText}
                      containerBorderRadius={0}
                      options={ detergentOptions }
                      onSelection={ this.setSelectedDetergentOption.bind(this) }
                      selectedOption={ this.state.selectedDetergentOption }
                      extractText={ (option) => option.label }
                    />
                  </View>
                  <View style={[styles.inputWrap, styles.extraMargin]}>
                    <LvText style={styles.label}>{dressShirtText}</LvText>
                    <SegmentedControls
                      tint={'black'}
                      selectedBackgroundColor={LaundryVroomColors.darkBackground}
                      separatorTint={LaundryVroomColors.lightText}
                      containerBorderTint={LaundryVroomColors.lightText}
                      containerBorderRadius={0}
                      options={ dressShirtOptions }
                      onSelection={ this.setSelectedDressShirtOption.bind(this) }
                      selectedOption={ this.state.selectedDressShirtOption }
                      extractText={ (option) => option.label }
                    />
                  </View>
                  <View style={[styles.inputWrap, styles.extraMargin]}>
                    <LvText style={styles.label}>Laundry can be:</LvText>
                    <SegmentedControls
                      tint={'black'}
                      selectedBackgroundColor={LaundryVroomColors.darkBackground}
                      separatorTint={LaundryVroomColors.lightText}
                      containerBorderTint={LaundryVroomColors.lightText}
                      containerBorderRadius={0}
                      options={ doorOptions }
                      onSelection={ this.setSelectedDoorOption.bind(this) }
                      selectedOption={ this.state.selectedDoorOption }
                      extractText={ (option) => option.label }
                    />
                  </View>
              </View>
              <View style={styles.saveButton, styles.extraMargin}>
                {this.state.saveProcessing ? (
                    <View>
                      <LaundryVroomButton
                         style={[styles.button, this.props.style, styles.buttonBottom]}
                         icon=""
                         loading={this.state.saveProcessing}
                         caption=""
                       />
                    </View>
                  ) : (
                    <View>
                      <LaundryVroomButton
                         onPress={(formValid ? this._savePreferences.bind(this) : this._printError.bind(this))}
                         style={[styles.button, this.props.style, styles.buttonBottom]}
                         icon={
                          ""
                         }
                         loading={this.state.saveProcessing}
                         caption="Save"
                       />
                     </View>
                  )
                }
              </View>
              <View style={styles.signOutContainer}>
                <LaundryVroomButton
                   onPress={() =>
                     Alert.alert(
                      'Signing Off?',
                      'Are you sure you want to sign out?',
                      [
                        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        {text: 'OK', onPress: () => {
                            this._logout();
                         }
                        },
                      ]
                     )
                   }
                   style={[styles.button]}
                   caption="Sign Out"
                   type='light'
                 />
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
     </View>
    );
   }

   _logout() {
     LaundryVroomStorage.removeStorageItem(STORAGE_KEY);
     this._resetToLanding();
   }

   _resetToLanding() {
     const resetAction = NavigationActions.reset({
       index: 0,
       actions: [NavigationActions.navigate({ routeName: 'Landing' })],
     });
     let {dispatch} = this.props.navigation;
     dispatch(resetAction);
   }
   _printError() {
     var message = "Please correct the following:\n";
     const { firstName,lastName,email,phone,password} = this.state;
     const firstNameValid = (firstName && firstName.length > 0 ? true : false);
     const lastNameValid = (lastName && lastName.length > 0 ? true : false);
     const passwordValid = (password && password.length > 0 ? true : false);
     const emailValid = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);
     const phoneValid = true;
     if(!firstNameValid) {
       message += "First Name must be set.\n";
     }
     if(!lastNameValid) {
       message += "Last Name must be set.\n";
     }
     if(!email) {
       message += "Email must be valid.\n";
     }
     if(!phoneValid) {
       message += "Phone number must be valid.\n";
     }
     if(!passwordValid) {
       message += "Please set a password greater than 6 characters with at least 1 symbol and 1 number.\n";
     }
     this.setState({error:true, errorMessage:message});
   }

   _savePreferences() {
      this.setState({saveProcessing:true, error:false, errorMessage:''});

      var customer = this.props.customer;
      customer.firstName = this.state.firstName;
      customer.lastName = this.state.lastName;
      customer.email = this.state.email;
      customer.phone = this.state.phone;

      var customerPreferences = this.props.customerPreferences;
      customerPreferences.detergentType = this.state.selectedDetergentOption.value; //"SCENTED",
      customerPreferences.dropOffAtDoor = this.state.selectedDoorOption.value; //true,
      customerPreferences.mensDressShirts = this.state.selectedDressShirtOption.value;  //"DRYCLEAN"

      this.props.dispatch(updateCustomer(customer.id, customer)).then(function(customer) {
        if(this.props.customer && this.props.customer.id) {
           this.props.dispatch(updateCustomerPreferences(customerPreferences.id, this.props.customer.id, customerPreferences)).then(function() {
             if(this.props.customerPreferences && this.props.customerPreferences.id) {
               this.props.navigation.navigate("Drawer");
             } else {
               if(this.props.customerPreferences && this.props.customerPreferences.message) {
                 this.setState({saveProcessing:false, error:true, errorMessage: this.props.customerPreferences.message});
               } else {
                  this.setState({saveProcessing:false, error:true, errorMessage: "An unknown error occurred, please try again."});
               }
             }
           }.bind(this));
       } else {
         if(this.props.customer && this.props.customer.message) {
           this.setState({saveProcessing:false, error:true, errorMessage: this.props.customer.message});
         } else {
            this.setState({saveProcessing:false, error:true, errorMessage: "An unknown error occurred, please try again."});
         }
       }
      }.bind(this));
   }

   _setPreferences() {
     if(this.props.customerPreferences) {
       for (var i = 0; i < detergentOptions.length; i++) {
           if(detergentOptions[i].value == this.props.customerPreferences.detergentType) {
             this.setSelectedDetergentOption(detergentOptions[i]);
           }
       }
       for (var i = 0; i < dressShirtOptions.length; i++) {
           if(dressShirtOptions[i].value == this.props.customerPreferences.mensDressShirts) {
             this.setSelectedDressShirtOption(dressShirtOptions[i]);
           }
       }
       for (var i = 0; i < doorOptions.length; i++) {
           if(doorOptions[i].value == this.props.customerPreferences.dropOffAtDoor) {
             this.setSelectedDoorOption(doorOptions[i]);
           }
       }
     }
   }

   setSelectedDetergentOption(selectedDetergentOption){
     this.setState({
       selectedDetergentOption
     });
   }

   setSelectedDressShirtOption(selectedDressShirtOption){
     this.setState({
       selectedDressShirtOption
     });
   }

   setSelectedDoorOption(selectedDoorOption){
     this.setState({
       selectedDoorOption
     });
   }

}

var styles = StyleSheet.create({
  form: {
    flex:1,
    backgroundColor:LaundryVroomColors.backgroundColor,
    opacity:1
  },
  name: {
    flex:1,
    flexDirection:'row',
  },
  grid: {
    margin:20
  },
  center: {
    alignSelf:'center'
  },
  gridRow: {
    flexDirection:'row',
  },
  noBottom: {
    borderStyle:'solid',
    borderBottomWidth:0,
    borderBottomColor:'white',
    borderLeftColor:'gray',
    borderLeftWidth:1,
    borderRightColor:'gray',
    borderRightWidth:1,
    borderTopColor:'gray',
    borderTopWidth:1,
  },
  fullBorder: {
    borderColor: 'gray',
    borderWidth: 1,
  },
  textInput: {
    height: 40,
    color:LaundryVroomColors.lightText,
    padding:10,
  },
  inputWrap: {
    flex:1,
    flexDirection:'column',
    marginBottom:10
  },
  extraMargin: {
    marginTop:10
  },
  label: {
    margin:3,
    alignSelf:'center',
    fontWeight:'bold',
    fontSize:12
  },
  selectStyle: {
    flex:2,
    flexDirection:'row'
  },
  saveButton: {
    flex:1,
    marginTop:15
  },
  processingContainter: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  errorContainer: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  error: {
    color: LaundryVroomColors.errorText
  },
  signOutContainer: {
    marginTop:10
  }
});

function select(store) {
  return {
    customer: store.customer,
    customerPreferences: store.customerPreferences,
  };
}

module.exports = connect(select)(Account);
