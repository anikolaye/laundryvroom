/**
* @providesModule OrderHistory
* @flow
*/
import React, {
  Component
} from 'react';

import {
  StyleSheet,
  View,
  ListView,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

const { connect } = require('react-redux');

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
// const Navigator = require('Navigator');
var { USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

import type {Customer} from '../reducers/customer';
import type {MerchantLocation} from '../reducers/merchantLocation';
import type {Orders} from '../reducers/orders';
import type {Dispatch} from '../actions/types';

import Icon from 'react-native-vector-icons/FontAwesome';
import Loading from '../common/Loading';
type Props = {
  customer: Customer;
  orders: any;
  merchantLocation: MerchantLocation;
  navigator: any;
  dispatch: Dispatch;
};

var {
  findOrders,
  findMerchants
} = require('../../actions');

class OrderHistory extends Component {
  static navigationOptions = ({navigation}) =>({
    title:'Order History',
    headerLeft:(
      <TouchableOpacity
         onPress={() => navigation.navigate('DrawerOpen')}
         style={{"marginLeft":10}}
       >
         <Icon name="bars" size={30} color="#fff" />
      </TouchableOpacity>
    ),
    headerRight:(
      <TouchableOpacity
         onPress={() => navigation.navigate('Drawer')}
         style={{"marginRight":10}}
       >
         <Icon name="map" size={30} color="#fff" />
      </TouchableOpacity>
    )
  });
  props: Props;

  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      loading:true,
      orders: ds.cloneWithRows({}),
    };
  }

  componentDidMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        navigate("Landing");
      }
    }.bind(this));

    let {customer} = this.props;
    if(!customer) {
      this.props.dispatch(loadCustomer(user.id));
    }
    this.props.dispatch(findOrders(this.props.customer.id, null)).then(() => {
      if(this.props.orders && this.props.orders.length > 0) {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({orders: ds.cloneWithRows(this.props.orders), loading:false});
      } else {
        this.setState({orders:null, loading:false});
      }

    });

  }

  render () {
     return (
       <View style={styles.parentContainer}>
         <View style={styles.orders}>
            {this.state.loading ? this.renderLoading() :this.renderOrders()}
         </View>
       </View>
     );
   }

   renderOrders() {
     if(this.state.orders) {
       return (
         <ListView
          style={styles.ordersContainer}
          dataSource={this.state.orders}
          renderRow={this._renderRow.bind(this)}
          renderSeparator={this._renderSeparator}
          contentInset={{bottom:49}}
          enableEmptySections={true}
        />
      );
    } else {
      return(
         <TouchableOpacity onPress={() => { this.props.navigation.navigate('Drawer')}}>
           <LvText>No Orders Yet! Click here to get your laundry done without the hassle!</LvText>
         </TouchableOpacity>
      );
    }
  }
  renderLoading() {
     return (
       <Loading />
     );
   }

   _renderRow(order) {
     if(order && order.invoiceId && order.status && order.status != 'INCOMPLETE') {
       var created = new Date(order.created);
       // Will display time in 10:30:23 format
       var formattedCreatedDate = (created.getMonth()+1) + '/' + created.getDate() + '/' + created.getFullYear();
       var formattedCreatedTime = (((created.getHours() + 11) % 12) + 1) + ':' + (created.getMinutes() < 10 ? '0' : '') + created.getMinutes() + ' ' + (created.getHours() > 11 ? 'PM' : 'AM');

       var lastModified = new Date(order.lastModified);
       // Will display time in 10:30:23 format
       var formattedModifiedDate = (lastModified.getMonth()+1) + '/' + lastModified.getDate() + '/' + lastModified.getFullYear();
       var formattedModifiedTime = (((lastModified.getHours() + 11) % 12) + 1) + ':' + (lastModified.getMinutes() < 10 ? '0' : '') + lastModified.getMinutes() + ' ' + (lastModified.getHours() > 11 ? 'PM' : 'AM');
       var redStatus = false;
       if(order.status && (order.status == 'INCOMPLETE' || order.status == 'CANCELLED')) {
         redStatus = true;
       }
        return (
          <TouchableOpacity style={styles.orderContainer} onPress={this._goLineItem.bind(this, order)}>
            <View style={styles.left}>
              <LvText style={styles.date}>{formattedCreatedDate} {formattedCreatedTime}</LvText>
              <LvText>Purchase Order: {order.invoiceId}</LvText>
              <LvText style={(redStatus ? styles.red : styles.green)}>{order.status}</LvText>
              <LvText>${order.price.toFixed(2)}</LvText>
           </View>
           <View style={styles.right}>
            <Icon style={styles.rightArrowButton} name="chevron-right" size={23} color={LaundryVroomColors.darkBackground}/>
           </View>
          </TouchableOpacity>
        );
      } else {
        return null;
      }
   }

   _renderSeparator(sectionID: number, rowID: number, adjacentRowHighlighted: bool) {
    return (
      <View
        key={`${sectionID}-${rowID}`}
        style={{
          height: adjacentRowHighlighted ? 4 : 1,
          backgroundColor: adjacentRowHighlighted ? '#3B5998' : '#CCCCCC',
        }}
      />
    );
  }

   _goLineItem(order) {
     let { navigate }= this.props.navigation;
     navigate("OrderLineHistory", {currentOrder:order});
   }
}

var styles = StyleSheet.create({
  title: {
    margin:10,
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  parentContainer: {
    flex:1,
    backgroundColor:'#fff'
  },
  orderContainer: {
    flex:1,
    flexDirection:'row',
    padding:10
  },
  orders: {
    flex:1,
    flexDirection: 'column',
  },
  ordersContainer: {
    height:600
  },
  left: {
    alignItems:'flex-start',
  },
  right: {
    flex:1,
    width:40,
    alignSelf:'center',
    justifyContent:'center',
    alignItems:'flex-end',
  },
  thumbnail: {
    width:50,
    height:20
  },
  date: {
    fontStyle:'italic'
  },
  red: {
    color:LaundryVroomColors.red,
  },
  green: {
    color:LaundryVroomColors.green,
  },
});

function select(store) {
  return {
    customer: store.customer,
    merchantLocation: store.merchantLocation,
    orders:store.orders
  };
}

module.exports = connect(select)(OrderHistory);
