/**
* @providesModule OrderLineHistory
* @flow
*/
import React, {
  Component
} from 'react';

import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Modal
} from 'react-native';

var { LvText } = require('LaundryVroomText');

const { connect } = require('react-redux');

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
// const Navigator = require('Navigator');
var { USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

const SingleOrderLine = require('SingleOrderLine');

import type {Customer} from '../reducers/customer';
import type {MerchantLocation} from '../reducers/merchantLocation';
import type {Order, OrderLine} from '../reducers/order';
import type {Dispatch} from '../actions/types';

import Icon from 'react-native-vector-icons/FontAwesome';
import Loading from '../common/Loading';

type Props = {
  customer: Customer;
  order: Order;
  orderlines: OrderLine;
  merchantLocation: MerchantLocation;
  navigator: any;
  dispatch: Dispatch;
};

var {
  findOrders,
  loadOrderLines,
  loadMerchantLocation
} = require('../../actions');

class OrderLineHistory extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Order ' + navigation.state.params.currentOrder.invoiceId,
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{"marginLeft":10}}
        >
          <Icon name="chevron-left" size={30} color="#fff" />
       </TouchableOpacity>
     ),
  });

  props: Props;

  constructor(props) {
    super(props);
    this.state = {
      loading:true,
      currentOrder:this.props.navigation.state.params.currentOrder,
      orderLines:null,
      merchantLocation:null
    };
  }

  componentDidMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then((customerId) => {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        navigate("Landing");
      }
    });
    let {currentOrder} = this.props.navigation.state.params;
    this.props.dispatch(loadOrderLines(currentOrder.id)).then(() => {
      //just grab the first merchant location as we send to one for now
      //TODO: support multiple merchant locations per order line?
      this.setState({orderLines:this.props.orderLines, loading:false});
    });

  }

  render () {
     const { loading, orderLines } = this.state;
     var address = null;
     var orderLineRows = null;
     if(orderLines != null) {
       address = orderLines[0].returnAddress;
       orderLineRows = orderLines.map(
          function(orderLine){
           return <SingleOrderLine key={orderLine.id} orderLine={orderLine} />;
          }
        );
      }
     if(loading) {
       return <Loading />
     } else {
       return (
         <View style={styles.parentContainer}>
           <ScrollView style={styles.historyContainer}>
            {orderLines ? (
              <View>
                {address ? (
                  <View style={styles.addressContainer}>
                    <View style={styles.deliveryContainer}>
                      <LvText style={styles.name}>
                        Picking Up From
                      </LvText>
                      <LvText style={styles.addressLine}>
                        {address.addressLine1}
                      </LvText>
                      <LvText style={styles.addressCity}>
                       {address.city}, {address.state} {address.zip}
                      </LvText>
                    </View>
                  </View>
                ) : null }
                <View>
                  {orderLineRows}
                </View>
              </View>
            ) : null }
           </ScrollView>
         </View>
       );
     }
   }
}

var styles = StyleSheet.create({
  parentContainer: {
    flex:1,
    backgroundColor:LaundryVroomColors.white
  },
  historyContainer: {
    padding:10
  },
  title: {
    margin:10,
    alignItems:'center',
    justifyContent:'center',
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  orderContainer: {
    marginTop: 10,
    marginBottom:10,
    padding:10,
    flex:1,
    flexDirection:'column',
    alignItems: 'flex-start',
    borderTopColor: LaundryVroomColors.cellBorder,
    borderTopWidth: 1,
    borderBottomColor: LaundryVroomColors.cellBorder,
    borderBottomWidth:1
  },
  addressContainer: {
    flexDirection:'row'
  },
  deliveryContainer: {
    marginTop:20,
    width:200,
    alignSelf:'flex-start',
    justifyContent: 'flex-start'
  },
  name: {
    paddingBottom:5,
    fontWeight:'bold'
  },
  storeName: {
    fontStyle:'italic'
  }
});

function select(store) {
  return {
    customer: store.customer,
    merchantLocation: store.merchantLocation,
    order: store.order,
    orderLines:store.orderLines
  };
}

module.exports = connect(select)(OrderLineHistory);
