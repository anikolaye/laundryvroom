/**
* @providesModule Registration
* @flow
*/
import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

const { connect } = require('react-redux');

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { Fumi } from 'react-native-textinput-effects';

import type {User} from '../reducers/user';
import type {Customer} from '../reducers/customer';
import type {CustomerPreferences} from '../reducers/customerPreferences';
import type {Dispatch} from '../actions/types';

var { STORAGE_KEY, DEVICE_ID, USER_ID} = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');
const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
// const Navigator = require('Navigator');

const dismissKeyboard = require('dismissKeyboard');

type Props = {
  customer: Customer;
  customerPreferences: CustomerPreferences;
  navigator: any;
  dispatch: Dispatch;
  onLogin: () => void;
};

var {
  authenticate,
  register,
  createCustomer,
  createCustomerPreferences
} = require('../../actions');

class Registration extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Register',
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.navigate("Landing")}
          style={{"marginLeft":10}}
        >
          <FontAwesomeIcon name="chevron-left" size={30} color="#fff" />
       </TouchableOpacity>
     ),
  });
  props: Props;

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone:'',
      password:'',
      registrationProcessing:false,
      error:false,
      errorMessage:'',
      formValid:false
    }
  }

  handleValueChange(values) {
    this.setState({ form: values });
  }

  render () {
    const { firstName,lastName,email,phone,password} = this.state;
    const firstNameValid = (firstName && firstName.length > 0 ? true : false);
    const lastNameValid = (lastName && lastName.length > 0 ? true : false);
    const passwordValid = (password && password.length > 0 ? true : false);
    const emailValid = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);
    const phoneValid = true;
    const formValid = firstNameValid && lastNameValid && passwordValid && emailValid && phoneValid;
    return (
      <View style={styles.form}>
        <ScrollView keyboardShouldPersistTaps="always">
              <TouchableWithoutFeedback onPress={()=> dismissKeyboard()}>
                <View>
                  {this.state.error ? (
                    <View style={styles.errorContainer}>
                      <LvText style={styles.error}>
                        {this.state.errorMessage}
                      </LvText>
                    </View>
                  ) : null }
                </View>
              </TouchableWithoutFeedback>
              <View>
                <Fumi
                  ref={'FirstInput'}
                  returnKeyType = {"next"}
                  onSubmitEditing={(event) => {
                    this.refs.SecondInput.focus();
                  }}
                  blurOnSubmit={false}
                  label={'First Name'}
                  iconClass={MaterialIcon}
                  iconName={'account-circle'}
                  iconColor={LaundryVroomColors.darkBackground}
                  // this is used as backgroundColor of icon container view.
                  iconBackgroundColor={'#f2a59d'}
                  inputStyle={{ color: '#464949' }}
                  autoCorrect={false}
                  autoCapitalize='sentences'
                  autoCorrect={false}
                  onChangeText={(text) => { this.setState({firstName: text}) }}
                />
                <Fumi
                  ref={'SecondInput'}
                  returnKeyType = {"next"}
                  onSubmitEditing={(event) => {
                    this.refs.ThirdInput.focus();
                  }}
                  blurOnSubmit={false}
                  label={'Last Name'}
                  iconClass={MaterialIcon}
                  iconName={'account-circle'}
                  iconColor={LaundryVroomColors.darkBackground}
                  // this is used as backgroundColor of icon container view.
                  iconBackgroundColor={'#f2a59d'}
                  inputStyle={{ color: '#464949' }}
                  autoCorrect={false}
                  autoCapitalize='sentences'
                  autoCorrect={false}
                  onChangeText={(text) => { this.setState({lastName: text}) }}
                />
                <Fumi
                  ref={'ThirdInput'}
                  returnKeyType = {"next"}
                  onSubmitEditing={(event) => {
                    this.refs.FourthInput.focus();
                  }}
                  blurOnSubmit={false}
                  label={'Phone'}
                  iconClass={MaterialIcon}
                  iconName={'phone'}
                  iconColor={LaundryVroomColors.darkBackground}
                  // this is used as backgroundColor of icon container view.
                  iconBackgroundColor={'#f2a59d'}
                  inputStyle={{ color: '#464949' }}
                  keyboardType='numeric'
                  autoCorrect={false}
                  onChangeText={(text) => { this.setState({phone: text}) }}
                />
                <Fumi
                  ref={'FourthInput'}
                  returnKeyType = {"next"}
                  onSubmitEditing={(event) => {
                    this.refs.FifthInput.focus();
                  }}
                  blurOnSubmit={false}
                  label={'Email'}
                  iconClass={FontAwesomeIcon}
                  iconName={'envelope'}
                  iconColor={LaundryVroomColors.darkBackground}
                  // this is used as backgroundColor of icon container view.
                  iconBackgroundColor={'#f2a59d'}
                  inputStyle={{ color: '#464949' }}
                  autoCapitalize='none'
                  keyboardType='email-address'
                  autoCorrect={false}
                  onChangeText={(text) => { this.setState({email: text}) }}
                />
                <Fumi
                  ref={'FifthInput'}
                  returnKeyType = {"done"}
                  label={'Password'}
                  iconClass={MaterialIcon}
                  iconName={'vpn-key'}
                  iconColor={LaundryVroomColors.darkBackground}
                  // this is used as backgroundColor of icon container view.
                  iconBackgroundColor={'#f2a59d'}
                  inputStyle={{ color: '#464949' }}
                  secureTextEntry={true}
                  autoCapitalize='none'
                  autoCorrect={false}
                  style={{ borderColor: 'gray' }}
                  titleStyle={{ color: 'dimgray' }}
                  inputStyle={{ color: 'slategray' }}
                  onChangeText={(text) => { this.setState({password: text}) }}
                  onSubmitEditing={(event) => {
                    (formValid ? this._register.bind(this) : this._printError.bind(this))
                  }}
                />
            </View>
            <TouchableWithoutFeedback onPress={()=> dismissKeyboard()}>
              <View style={styles.signupButton, styles.gridItem}>
                {this.state.registrationProcessing ? (
                    <View>
                      <LaundryVroomButton
                         style={[styles.button, this.props.style, styles.buttonBottom]}
                         icon=""
                         loading={this.state.registrationProcessing}
                         caption=""
                       />
                    </View>
                  ) : (
                    <View>
                      <LaundryVroomButton
                         onPress={(formValid ? this._register.bind(this) : this._printError.bind(this))}
                         style={[styles.button, this.props.style, styles.buttonBottom]}
                         icon={
                          ""
                         }
                         loading={this.state.registrationProcessing}
                         caption="Sign Up"
                       />
                     </View>
                  )
                }
              </View>
            </TouchableWithoutFeedback>
        </ScrollView>
      </View>
   );
  }

  _printError() {
    var message = "Please correct the following:\n";
    const { firstName,lastName,email,phone,password} = this.state;
    const firstNameValid = (firstName && firstName.length > 0 ? true : false);
    const lastNameValid = (lastName && lastName.length > 0 ? true : false);
    const passwordValid = (password && password.length > 0 ? true : false);
    const emailValid = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);
    const phoneValid = true;
    if(!firstNameValid) {
      message += "First Name must be set.\n";
    }
    if(!lastNameValid) {
      message += "Last Name must be set.\n";
    }
    if(!email) {
      message += "Email must be valid.\n";
    }
    if(!phoneValid) {
      message += "Phone number must be valid.\n";
    }
    if(!passwordValid) {
      message += "Please set a password greater than 6 characters with at least 1 symbol and 1 number.\n";
    }
    this.setState({error:true, errorMessage:message});
  }

  _register() {
    this.setState({registrationProcessing:true, error:false, errorMessage:''});
    var user = {
      deviceId: DEVICE_ID,
      fullName: this.state.firstName + " " + this.state.lastName,
      password: this.state.password,
      userRoles: [
        {
          roleName: "CUSTOMER"
        }
      ],
      username: this.state.email
    };
    this.props.dispatch(register(user)).then(function(user) {
      if(this.props.user && this.props.user.id) {
        var customer = {
          firstName: this.state.firstName,
          lastName:this.state.lastName,
          email:this.state.email,
          phone:this.state.phone,
          password: this.state.password,
          userId: this.props.user.id
        }
        this.props.dispatch(createCustomer(customer)).then(function(customer) {
          if(this.props.customer && this.props.customer.id) {
            var customerPreferences = {
               customerId: customer.id,
               detergentType: "UNSCENTED",
               dropOffAtDoor: false, //true,
               mensDressShirts: "MERCHANT"
             }
             this.props.dispatch(createCustomerPreferences(this.props.customer.id, customerPreferences)).then(function() {
               LaundryVroomStorage.setStorageItem(USER_ID, `${this.props.customer.id}`).then(function() {
                  this._goMap();
               }.bind(this));
             }.bind(this));
         } else {
           if(this.props.customer && this.props.customer.message) {
             this.setState({registrationProcessing:false, error:true, errorMessage: this.props.customer.message});
           } else {
              this.setState({registrationProcessing:false, error:true, errorMessage: "An unknown error occurred, please try again."});
           }
         }
        }.bind(this));
      } else {
        if(this.props.user && this.props.user.message) {
          this.setState({registrationProcessing:false, error:true, errorMessage: this.props.user.message});
        } else {
           this.setState({registrationProcessing:false, error:true, errorMessage: "An unknown error occurred, please try again."});
        }
      }
    }.bind(this));
  }

  _goMap() {
    let {navigate} = this.props.navigation;
    navigate('Drawer');
  }
}

var styles = StyleSheet.create({
  form: {
    flex:1,
    backgroundColor:LaundryVroomColors.backgroundColor
  },
  grid: {
    margin:20
  },
  registrationForm: {
    marginBottom:20
  },
  center: {
    alignSelf:'center'
  },
  gridRow: {
    flexDirection:'row',
  },
  noBottom: {
    borderStyle:'solid',
    borderBottomWidth:0,
    borderBottomColor:'white',
    borderLeftColor:'gray',
    borderLeftWidth:1,
    borderRightColor:'gray',
    borderRightWidth:1,
    borderTopColor:'gray',
    borderTopWidth:1,
  },
  fullBorder: {
    borderColor: 'gray',
    borderWidth: 1,
  },
  textInput: {
    height: 40,
    color:LaundryVroomColors.lightText,
    padding:10,
  },
  loginButton: {
    flex:1,
    marginTop:15
  },
  processingContainter: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  errorContainer: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  error: {
    color: LaundryVroomColors.errorText
  }
});

function select(store) {
  return {
    user: store.user,
    customer: store.customer,
    customerPreferences: store.customerPreferences,
  };
}

module.exports = connect(select)(Registration);
