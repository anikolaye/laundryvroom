/**
* @providesModule OrderItem
* @flow
*/

import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

import type { Dispatch } from '../actions/types';

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');

class OrderItem extends Component {
  props: {
    image: string;
    name:string;
    sku:string;
    navigator: Navigator;
    onValueChange:any;
    instructions:string;
  };

  constructor(props) {
    super(props);
    this.state = {
      instructions:props.instructions,
    }
  }

  componentDidMount() {
    this.setState({instructions: this.props.instructions});
  }

  handleValueChange(value) {
    this.setState({instructions: value});
    this.props.onValueChange(value);
  }
  
  render () {
    return (
      <View style={{flex:1}}>
        <View style={styles.row}>
          <LvText numberOfLines={1} style={styles.name}>
            {this.props.name}
          </LvText>
        </View>
        <View style={styles.row}>
          <View style={styles.imageContainer}>
            {this.props.sku == 'WASH' ? (
              <Image
               style={styles.thumbnail}
               resizeMode={Image.resizeMode.contain}
               source={require('../../common/images/washer.png')}
              />
            ) :
              <Image
               style={styles.thumbnail}
               resizeMode={Image.resizeMode.contain}
               source={require('../../common/images/hanger_blue.png')}
              />
            }
          </View>
          <View style={styles.instructionsContainer}>
            {this.renderInstructions(this.props.sku)}
          </View>
        </View>
      </View>
   );
  }

  renderInstructions() {
    let {instructions} = this.state;
    let instructionsTitle = "Edit instructions";
    if(!instructions) {
      instructionsTitle = "Add Special Instructions";
    }

    return(
      <TouchableOpacity onPress={() => this._goInstructions()}>
        <LvText style={styles.instructionsTitle}>{instructionsTitle}</LvText>
        <LvText>{instructions}</LvText>
      </TouchableOpacity>
    );
  }

  _goInstructions() {
    let {navigate} = this.props.navigation;
    //Think we need to pass the order back and forth?  Not sure how to navigate
    navigate("InstructionsModal", {referrer:this.props.navigation.state.key, instructions:this.state.instructions, save:this.handleValueChange.bind(this)});
  }
}

var styles = StyleSheet.create({
  row: {
    backgroundColor:LaundryVroomColors.backgroundColor,
    opacity:1,
    flexDirection:'row'
  },
  imageContainer: {
    marginTop:8,
    width:75,
    alignSelf:'flex-start',
    justifyContent: 'flex-start'
  },
  instructionsContainer: {
    paddingBottom:10,
    maxWidth:180,
    alignSelf:'flex-end',
    justifyContent: 'flex-end'
  },
  instructionsTitle: {
    color:LaundryVroomColors.darkBackground,
    fontWeight:"bold",
  },
  textInput: {
    height: 40,
    color:LaundryVroomColors.lightText,
    padding:10,
  },
  thumbnail: {
    height:60,
    width:60
  },
  name: {
    paddingBottom:5,
    fontWeight:'bold'
  }
});

module.exports = OrderItem;
