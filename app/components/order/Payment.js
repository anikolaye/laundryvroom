/**
* @providesModule Payment
* @flow
*/

import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const { connect } = require('react-redux');

import type { Dispatch } from '../actions/types';

import type {Order} from '../reducers/order';

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
import Icon from 'react-native-vector-icons/FontAwesome';
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import BTClient from 'react-native-braintree-xplat';

var {
  createOrder,
  updateOrder,
  createOrderLines,
  loadCustomer,
  loadCustomerPreferences,
  loadPaymentToken,
  sendPayment,
  updateCustomer,
  createPaymentMethod
} = require('../../actions');

class Payment extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Payment',
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{"marginLeft":10}}
        >
          <Icon name="chevron-left" size={30} color="#fff" />
       </TouchableOpacity>
     ),
  });

  props: {
    order:Order;
    orderProcessing:Boolean;
    error:Boolean;
    errorMessage:String;
  };

  constructor(props) {
    super(props);
    this.state = {
      order:props.navigation.state.params.order,
      orderProcessing:false,
      error:false,
      errorMessage:'',
      inputValid:false,
      values:{
        number:'',
        expiry:'',
        cvc:'',
        type:''
      }
    }
  }

  componentDidMount() {
    this.setState({order:this.props.navigation.state.params.order});
  }

  render () {
    return (
      <View style={styles.paymentForm}>
        <View>
          <CreditCardInput onChange={(form) => {this._onChange(form)}} autoFocus={true}/>
        </View>
        <View style={styles.submitButton}>
          {(this.state.inputValid) ?
            <LaundryVroomButton
               onPress={(!this.state.orderProcessing) ? this._createOrder.bind(this) : null}
               style={[styles.button, this.props.style, styles.buttonBottom]}
               loading={this.state.orderProcessing}
               caption="Submit Payment"
             /> :
             <LaundryVroomButton
                type="inactive"
                style={[styles.inactiveButton, styles.buttonBottom]}
                caption="Submit Payment"
              />
           }
         </View>
      </View>
   );
  }

  _onChange(form) {
    this.setState({
      inputValid:form.valid,
      ccValues:form.values
    });
  }
  _createOrder() {
    if(!this.state.orderProcessing) { //If we didn't click it already
      this.setState({
        error:false,
        errorMessage:'',
        orderProcessing: true});
      this.props.dispatch(loadPaymentToken()).done(() => {
        BTClient.setupWithURLScheme(this.props.payment.token, 'org.laundryvroom.LaundryVroom.payments')
        .catch((err) => {
          this.setState({
            orderProcessing: false,
            error:true,
            errorMessage: 'Uh oh, we messed up, please try again!'
          });
        });

        let {expiry} = this.state.ccValues;
        var res = expiry.split("/"); //mm/yy
        BTClient.getCardNonce(this.state.ccValues.number, res[0], res[1], this.state.ccValues.cvc).then((nonce) => {
          //payment succeeded, pass nonce to server
          if(this.props.order.id) {
            this._unsetOrder();
          }
          if(this.props.order.orderPaymentMethod) {
            this.props.order.orderPaymentMethod = null;
          }
          this.props.order.billingAddress = this.state.order.address;
          this.props.order.price = this.state.order.price;
          this.props.order.requesterId = this.props.customer.id;
          this.props.order.status = "INCOMPLETE";
          this.props.order.type= 'CUSTOMER';
          this.props.dispatch(createOrder(this.props.order)).then(() => {
            if(this.props.order.id) {
              const paymentMethodLocal = {};
              paymentMethodLocal.nonce = nonce;
              paymentMethodLocal.transactionId = this.props.order.id;
              paymentMethodLocal.firstName = this.props.customer.firstName;
              paymentMethodLocal.lastName = this.props.customer.lastName;
              paymentMethodLocal.email = this.props.customer.email;
              paymentMethodLocal.phone = this.props.customer.phone;
              this.props.dispatch(createPaymentMethod(paymentMethodLocal)).then(() => {

                var orderLines = this._getOrderLines();
                this.props.dispatch(createOrderLines(this.props.order.id, orderLines)).then(() => {
                  this.props.order.status = "PROCESSING";
                  this.props.order.orderPaymentMethod = this.props.paymentMethod;
                  this.props.dispatch(updateOrder(this.props.order.id, this.props.order)).then(() => {
                    //redirect after updated
                    this._goSuccess(this.props.order, orderLines, this.state.order.merchantLocation);
                  });
                });
                //create line items and update order.
              })
              .catch((err) => {
                this._unsetOrder();
                this.setState({
                  orderProcessing: false,
                  error:true,
                  errorMessage: 'Uh oh, there was an error sending the payment, please try again!'
                });
                console.log("Failed to send payment", err);
              });
            } else {
              this.setState({
                orderProcessing: false,
                error:true,
                errorMessage: 'Uh oh, there was an error with your order, please try again.'
              });
              console.log("Failed to create order");
            }
          })
          .catch((err) => {
            console.log("Error", err);
            this.setState({
              orderProcessing: false,
              error:true,
              errorMessage: 'Uh oh, there was an error with your order, please try again.'
            });
          });
        })
        .catch((err) => {
          console.log("nonce error?", err);
          this.setState({
            orderProcessing: false,
            error:true,
            errorMessage:"Error occurred during payment with message: " + err
          });
          //error handling
        });
      });
    }
  }

  _getOrderLines() {
    var orderLines = [];
    if(this.state.order.products) {
      if(this.state.order.products.WASH.selected) {
        orderLineProductAttributes = [];
        if(this.state.order.products.WASH.instructions) {
          orderLineProductAttribute = {
            key: 'Special Instructions',
            value: this.state.order.products.WASH.instructions
          };
          orderLineProductAttributes.push(orderLineProductAttribute);
        }
        orderLineProduct = {
          name: this.state.order.products.WASH.name,
          sku: this.state.order.products.WASH.productSku,
          description: this.state.order.products.WASH.description,
          orderLineProductAttributes: orderLineProductAttributes
        };
        orderLine = this._generateOrderLine(orderLineProduct);
        orderLines.push(orderLine);
      }
      if(this.state.order.products.DRYCLEAN.selected) {
        //Get product data.
        orderLineProductAttributes = [];
        if(this.state.order.products.DRYCLEAN.instructions) {
          orderLineProductAttribute = {
            key: 'Special Instructions',
            value: this.state.order.products.DRYCLEAN.instructions
          };
          orderLineProductAttributes.push(orderLineProductAttribute);
        }
        orderLineProduct = {
          name: this.state.order.products.DRYCLEAN.name,
          sku: this.state.order.products.DRYCLEAN.productSku,
          description: this.state.order.products.DRYCLEAN.description,
          orderLineProductAttributes: orderLineProductAttributes
        };
        orderLine = this._generateOrderLine(orderLineProduct);
        orderLines.push(orderLine);
      }
    } else {
      console.log("No products were set!!");
    }
    return orderLines;
  }

  _generateOrderLine(orderLineProduct) {
    return {
      orderId: this.props.order.id,
      status: "PICKUP_REQUESTED",
      price: this.state.order.price,
      merchantLocationId: this.state.order.merchantLocation.id,
      customerId: this.props.customer.id,
      destinationName:this.state.order.merchantLocation.storeName,
      destinationPhone: this.state.order.merchantLocation.storePhone,
      destinationAddress: this.state.order.merchantLocation.storeAddress,
      returnFirstName:this.props.customer.firstName,
      returnLastName:this.props.customer.lastName,
      returnPhone:this.props.customer.phone,
      returnAddress: this.state.order.address,
      orderLineProduct:orderLineProduct
    };
  }

  _unsetOrder() {
    if(this.props && this.props.order && this.props.order.id) {
      this.props.order.id = null;
      this.props.order.orderPaymentMethod = null;
    }
  }

  _goSuccess(order, orderLines, merchantLocation) {
    this.setState({orderProcessing: false});
    this.props.navigation.navigate("OrderSuccess",{
      orderSuccess:true,
      order:order,
      orderLines: orderLines,
      merchantLocation: merchantLocation
    });
  }
}

var styles = StyleSheet.create({
  paymentForm: {
    flex:1
  },
  submitButton: {
    marginTop:20
  }
});

function select(store) {
  return {
    customer: store.customer,
    order: store.order,
    payment: store.payment,
    paymentMethod:store.paymentMethod
  };
}

module.exports = connect(select)(Payment);
