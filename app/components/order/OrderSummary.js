/**
* @providesModule OrderSummary
* @flow
*/
import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  View,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');
import Loading from '../common/Loading';

import Icon from 'react-native-vector-icons/FontAwesome';

var { USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

const OrderItem = require('OrderItem');

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');

const ProductSku = {
  WASH: "WASH",
  DRYCLEAN: "DRYCLEAN"
};

type Props = {
  order: Order;
};

class OrderSummary extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Order Summary',
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{"marginLeft":10}}
        >
          <Icon name="chevron-left" size={30} color="#fff" />
       </TouchableOpacity>
     ),
  });

  props: Props;

  constructor(props) {
    super(props);
    this.state = {
      washInstructions:'',
      drycleanInstructions:'',
      order: this.props.navigation.state.params.order,
      orderProcessing: false,
      merchantLocation: null,
      error: false,
      errorMessage: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        navigate("Landing");
      }
    }.bind(this));
  }

  handleWashInstructions(value) {
    let {order} = this.state;
    order.products.WASH.instructions = value;
    this.setState({ order: order, washInstructions: value });
  }

  handleDrycleanInstructions(value) {
    let {order} = this.state;
    order.products.DRYCLEAN.instructions = value;
    this.setState({ order: order, drycleanInstructions: value });
  }

  render () {
    var order = this.state.order ? this.state.order : this.props.navigation.state.params.order;

    const {products,address,merchantLocation, price} = order;
    const {navigator} = this.props;
    if(address && products && merchantLocation) {
      return (
        <View style={styles.form}>
            <ScrollView style={styles.grid}>
              {this.state.error ? (
                <View style={styles.errorContainer}>
                  <LvText style={styles.error}>
                    {this.state.errorMessage}
                  </LvText>
                </View>
              ) : null }
              <View style={styles.addressContainer}>
                <View style={styles.deliveryContainer}>
                  <LvText style={styles.name}>
                    Pickup and Delivery
                  </LvText>
                  <LvText style={styles.addressLine}>
                    {address.addressLine1}
                  </LvText>
                  <LvText style={styles.addressCity}>
                   {address.city}, {address.state} {address.zip}
                  </LvText>
                </View>
                {this.state.renderAddressUpdate ? (
                  <View style={styles.saveContainer}>
                    <LaundryVroomButton
                       onPress={this._saveAddress.bind(this)}
                       style={[styles.button]}
                       caption="Set as Default"
                       type='light'
                     />
                  </View>
                ) : null }
                {this.state.renderCheck ? (
                  <View style={styles.saveContainer}>
                  </View>
                ) : null }
              </View>
              {merchantLocation ? (
                <View style={styles.addressContainer}>
                  <View style={styles.deliveryContainer}>
                    <LvText style={styles.name}>
                      Sending To
                    </LvText>
                    <LvText style={styles.storeName}>
                      {merchantLocation.storeName}
                    </LvText>
                    <LvText style={styles.addressLine}>
                      {merchantLocation.storeAddress.addressLine1}
                    </LvText>
                    <LvText style={styles.addressCity}>
                     {merchantLocation.storeAddress.city}, {merchantLocation.storeAddress.state} {merchantLocation.storeAddress.zip}
                    </LvText>
                  </View>
                </View>
              ) : null}
               <View>
                 {products.WASH.selected ? (
                   <OrderItem
                     image='../../common/images/washer.png'
                     name='Wash & Fold'
                     sku={ProductSku.WASH}
                     instructions={this.state.washInstructions}
                     onValueChange={this.handleWashInstructions.bind(this)}
                     navigation={this.props.navigation}
                   />
                 ) : null}
                 {products.DRYCLEAN.selected ? (
                   <OrderItem
                     image='../../common/images/hanger_blue.png'
                     name='Dry Clean'
                     sku={ProductSku.DRYCLEAN}
                     instructions={this.state.drycleanInstructions}
                     onValueChange={this.handleDrycleanInstructions.bind(this)}
                     navigation={this.props.navigation}
                   >
                  </OrderItem>
                 ) : null }
               </View>
              <View style={styles.paymentContainer}>
                <LvText>
                  Minimum Payment: ${price} <TouchableOpacity style={styles.pricing} onPress={this._goPricing.bind(this)}><Icon name="question-circle" size={15} color={LaundryVroomColors.darkBackground}/></TouchableOpacity>
                </LvText>
              </View>
              <View style={styles.paymentButton}>
                <LaundryVroomButton
                 onPress={this._goPayment.bind(this)}
                 style={[styles.button, this.props.style, styles.buttonBottom]}
                 loading={this.state.saveProcessing}
                 caption="Proceed to Payment"
               />
              </View>
            </ScrollView>
        </View>
      );
   } else {
     return (<Loading />);
   }
  }

  _goPricing() {
    let {navigate, state} = this.props.navigation;
    navigate("Pricing", {order:state.params.order, referrer:'Summary'});
  }

  _goPayment() {
    let {navigate, state} = this.props.navigation;
    navigate("Payment", {order:state.params.order});
  }
}

var styles = StyleSheet.create({
  form: {
    flex:1,
    backgroundColor:LaundryVroomColors.backgroundColor,
    opacity:1
  },
  grid: {
    marginLeft:20,
    marginRight:20,
    marginBottom:20
  },
  gridRow: {
    flexDirection:'row',
  },
  addressContainer: {
    flexDirection:'row'
  },
  deliveryContainer: {
    marginTop:20,
    width:200,
    alignSelf:'flex-start',
    justifyContent: 'flex-start'
  },
  saveContainer: {
    width:125,
    marginRight:25,
    alignSelf:'flex-end',
    justifyContent: 'flex-end'
  },
  button: {
    backgroundColor:LaundryVroomColors.backgroundColor,
    borderColor:LaundryVroomColors.lightText
  },
  textInput: {
    height: 40,
    color:LaundryVroomColors.lightText,
    padding:10,
  },
  name: {
    paddingBottom:5,
    fontWeight:'bold'
  },
  note: {
    color:LaundryVroomColors.actionText
  },
  paymentContainer: {
    borderTopWidth:1,
    borderColor:LaundryVroomColors.lightText,
    paddingTop:10
  },
  processingContainter: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  errorContainer: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  error: {
    color: LaundryVroomColors.errorText
  },
  storeName: {
    fontStyle:'italic'
  },
  order: {
    borderTopWidth:1,
    borderColor:LaundryVroomColors.lightText,
    paddingTop:10,
    marginTop:10
  },
  pricing: {
    width:50,
    height:14,
    paddingLeft:7
  },
  paymentButton: {
    marginTop:10
  }
});

module.exports = OrderSummary;
