/**
* @providesModule ProductSelection
* @flow
*/
import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

import type { Dispatch } from '../actions/types';
const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
import LaundryVroomOption from '../common/LaundryVroomOption';
// const Navigator = require('Navigator');
var { USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

const ProductSku = {
  WASH: "WASH",
  DRYCLEAN: "DRYCLEAN"
};

import Icon from 'react-native-vector-icons/FontAwesome';

class ProductSelection extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Select Services',
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{"marginLeft":10}}
        >
          <Icon name="chevron-left" size={30} color="#fff" />
       </TouchableOpacity>
     ),
  });

  constructor(props) {
    super(props);
    this.state = {
      form: {
        wash: false,
        dryclean:false
      },
      products: {
        WASH: {
          selected:false,
          name: "Wash & Fold",
          productSku: "WASH",
          description: "Wash & Fold",
        },
        DRYCLEAN:{
          selected:false,
          name: "Dry Clean",
          productSku: "DRYCLEAN",
          description: "Dry Clean",
        }
      },
      note: '',
      error: false,
      errorMessage: ''
    }
  }

  componentDidMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      let {navigate} = this.props.navigation;
      if(customerId === null) {
        navigate("Landing");
      }
    }.bind(this));

    var form = this.state;
    var order = this.props.navigation.state.params.order;
    if(form && form.wash && form.dryclean) {
      this.setState({
        form: form,
        note:'Please note by selecting both, return service will typically be on different days.',
        order:order
      });
    } else {
      this.setState({
        form: form,
        note:'',
        order:order
      });
    }
  }

  handleValueChange(wash, dryclean) {
    let form = { wash:wash, dryclean:dryclean};
    let note = '';
    let {products, error, errorMessage} = this.state;
    if(this.state.error && (wash || dryclean)) {
      error =false;
      errorMessage = '';
    }
    if(wash) {
      products.WASH.selected = true;
    } else {
      products.WASH.selected = false;
    }
    if(dryclean) {
      products.DRYCLEAN.selected = true;
    } else {
      products.DRYCLEAN.selected = false;
    }
    if(wash && dryclean) {
      note = 'Please note by selecting both, return service will typically be on different days.';
    }
    this.setState({
      form:form,
      products:products,
      error:error,
      errorMessage: errorMessage,
      note:note
    });
  }

  render () {
    let {wash, dryclean} = this.state.form;
    return (
      <View style={styles.form}>
          <View style={styles.grid}>
            {this.state.error ? (
              <View style={styles.errorContainer}>
                <LvText style={styles.error}>
                  {this.state.errorMessage}
                </LvText>
              </View>
            ) : null }
              <LaundryVroomOption
                name={ProductSku.WASH} // mandatory
                title='Wash & Fold'
                _onChange={(value) => { this.handleValueChange(value, dryclean)}}
              />

              <LaundryVroomOption
                name={ProductSku.DRYCLEAN} // mandatory
                title='Dry Clean'
                _onChange={(value) => {this.handleValueChange(wash, value)}}
              />
              <LvText style={styles.note}>{this.state.note}</LvText>
              <View style={styles.continueButton}>
                {(wash || dryclean) ?
                  <LaundryVroomButton
                     onPress={this._goSummary.bind(this)}
                     style={[styles.button, this.props.style, styles.buttonBottom]}
                     icon={
                      ""
                     }
                     loading={this.state.saveProcessing}
                     caption="Continue"
                   /> :
                   <LaundryVroomButton
                      type="inactive"
                      style={[styles.inactiveButton, styles.buttonBottom]}
                      caption="Continue"
                    />
                 }
               </View>
          </View>
      </View>
   );
  }

  _goSummary() {
    let {products, order} = this.state;
    var error = true;
    if(this.state.form.wash) {
      error = false;
      products.WASH.selected = true;
    } else {
      products.WASH.selected = false;
    }
    if(this.state.form.dryclean) {
      error = false;
      products.DRYCLEAN.selected = true;
    } else {
      products.DRYCLEAN.selected = false;
    }
    var errorMessage = '';
    if(error) {
      errorMessage = 'Please choose a service';
    }
    this.setState({ products: products, error:error, errorMessage:errorMessage });
    if(!error) {
      order['products'] = products;
      order['price'] = 30.00
      let {navigate} = this.props.navigation;
      navigate("OrderSummary", {order:order});
    }
  }
}

var styles = StyleSheet.create({
  form: {
    flex:1,
    backgroundColor:LaundryVroomColors.backgroundColor,
    opacity:1
  },
  grid: {
    margin:20
  },
  center: {
    alignSelf:'center'
  },
  gridRow: {
    flexDirection:'row',
  },
  noBottom: {
    borderStyle:'solid',
    borderBottomWidth:0,
    borderBottomColor:'white',
    borderLeftColor:'gray',
    borderLeftWidth:1,
    borderRightColor:'gray',
    borderRightWidth:1,
    borderTopColor:'gray',
    borderTopWidth:1,
  },
  fullBorder: {
    borderColor: 'gray',
    borderWidth: 1,
  },
  textInput: {
    height: 40,
    color:LaundryVroomColors.lightText,
    padding:10,
  },
  note: {
    color:LaundryVroomColors.actionText
  },
  errorContainer: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  error: {
    color: LaundryVroomColors.errorText
  },
  inactiveButton: {
    // color:LaundryVroomColors.white,
    backgroundColor:LaundryVroomColors.inactiveText
  },
  continueButton: {
    marginTop:10
  }
});

module.exports = ProductSelection;
