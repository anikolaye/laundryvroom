/**
* @providesModule OrderSuccess
* @flow
*/
import React, {
  Component
} from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Modal,
  ScrollView
} from 'react-native';

import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

var { LvText } = require('LaundryVroomText');

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');
// const Navigator = require('Navigator');
const SingleOrderLine = require('SingleOrderLine');

var { USER_ID } = require('../../env');
const LaundryVroomStorage = require('LaundryVroomStorage');

class OrderSuccess extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Success!',
     headerLeft:null,
     headerRight:(
       <TouchableOpacity
          onPress={() => navigation.navigate("Drawer")}
          style={{"marginRight":10}}
        >
          <Icon name="chevron-right" size={30} color="#fff" />
       </TouchableOpacity>
     ),
  });
  props: {
    // navigator: Navigator;
    onLogin: () => void;
    order: any;
    orderLines: any;
    merchantLocation: any;
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    LaundryVroomStorage.getStorageItem(USER_ID).then(function(customerId) {
      if(customerId === null) {
        this._resetToLanding();
      }
    }.bind(this));
  }

  render () {
    let {order, orderLines, merchantLocation} = this.props.navigation.state.params;
    var address = null;
    var orderLineRows = null;
    if(orderLines != null) {
      address = orderLines[0].returnAddress;
      orderLineRows = orderLines.map(
         function(orderLine, index){
          return <SingleOrderLine key={index} orderLine={orderLine} />;
         }
       );
     }

    return (
       <View style={styles.form}>
         <View style={styles.successContainer}>
           <View style={styles.title}>
             <LvText style={styles.titleText}>Vroom! A driver is on the way!</LvText>
           </View>
           <View style={styles.subTitle}>
             <LvText style={styles.subTitle}>Order {order.invoiceId}</LvText>
             <LvText style={styles.subTitle}>Minimum total: ${order.price.toFixed(2)}</LvText>
           </View>
            <View style={styles.description}>
              <LvText style={styles.description}>Please put your laundry in your LaundryVroom Bag.</LvText>
            </View>
         </View>
         <ScrollView  style={styles.successContainer}>
          {orderLines ? (
            <View>
              {address ? (
                <View style={styles.addressContainer}>
                  <View style={styles.deliveryContainer}>
                    <LvText style={styles.name}>
                      Picking Up From
                    </LvText>
                    <LvText style={styles.addressLine}>
                      {address.addressLine1}
                    </LvText>
                    <LvText style={styles.addressCity}>
                     {address.city}, {address.state} {address.zip}
                    </LvText>
                  </View>
                </View>
              ) : null }
              <View>
                {orderLineRows}
              </View>
            </View>
          ) : null }
         </ScrollView>
       </View>
     );
   }

   _resetToLanding() {
     const resetAction = NavigationActions.reset({
       index: 0,
       actions: [NavigationActions.navigate({ routeName: 'Landing' })],
     });
     let {dispatch} = this.props.navigation;
     dispatch(resetAction);
   }
}

var styles = StyleSheet.create({
  form: {
    flex:1,
    backgroundColor:LaundryVroomColors.backgroundColor
  },
  successContainer: {
    padding:10
  },
  title: {
    margin:10,
    alignItems:'center',
    justifyContent:'center',
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  orderContainer: {
    marginTop: 10,
    marginBottom:10,
    padding:10,
    flex:1,
    flexDirection:'column',
    alignItems: 'flex-start',
    borderTopColor: LaundryVroomColors.cellBorder,
    borderTopWidth: 1,
    borderBottomColor: LaundryVroomColors.cellBorder,
    borderBottomWidth:1
  },
  addressContainer: {
    flexDirection:'row'
  },
  deliveryContainer: {
    marginTop:20,
    width:200,
    alignSelf:'flex-start',
    justifyContent: 'flex-start'
  },
  name: {
    paddingBottom:5,
    fontWeight:'bold'
  },
  storeName: {
    fontStyle:'italic'
  }
});

module.exports = OrderSuccess;
