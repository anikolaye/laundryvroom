/**
* @providesModule OrderItemInstructions
* @flow
*/

import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import type { Dispatch } from '../actions/types';
import { NavigationActions } from 'react-navigation';

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
import Icon from 'react-native-vector-icons/FontAwesome';

class OrderItemInstructions extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Special Instructions',
     headerLeft:(
       <TouchableOpacity
          onPress={() => navigation.dispatch(NavigationActions.back())}
          style={{"marginLeft":10}}
        >
          <Icon name="chevron-down" size={30} color="#fff" />
       </TouchableOpacity>
     ),
  });

  props: {
  };

  constructor(props) {
    super(props);
    this.state = {
      value:props.navigation.state.params.instructions
    }
  }

  componentDidMount() {
    this.setState({value:this.props.navigation.state.params.instructions});
  }

  handleValueChange(value) {
    this.setState({value: value});
  }

  render () {
    return (
      <View>
        <View style={styles.textAreaRow}>
          <TextInput
            style={styles.textArea}
            multiline={true}
            autoFocus={true}
            {...this.props}
            onChangeText={this.handleValueChange.bind(this)}
            value={this.state.value}
          />
        </View>
        <View style={styles.saveButton}>
          {(this.state.value) ?
            <LaundryVroomButton
               onPress={this._save.bind(this)}
               style={[styles.button, this.props.style, styles.buttonBottom]}
               icon={
                ""
               }
               loading={this.state.saveProcessing}
               caption="Save"
             /> :
             <LaundryVroomButton
                type="inactive"
                style={[styles.inactiveButton, styles.buttonBottom]}
                caption="Save"
              />
           }
         </View>
      </View>
   );
  }

  _save() {
    this.props.navigation.state.params.save(this.state.value);
    this.props.navigation.dispatch(NavigationActions.back());
  }
}

var styles = StyleSheet.create({
  form: {
    flex:1,
    alignItems: 'center',
  },
  textAreaRow: {
    backgroundColor: '#FFF',
    height: 120,
    borderBottomWidth: 1,
    borderColor: '#c8c7cc',
    paddingLeft: 10,
    paddingRight: 10,
  },
  textArea: {
    fontSize: 15,
  },
});

module.exports = OrderItemInstructions;
