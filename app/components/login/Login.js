/**
* @providesModule Login
* @flow
*/
import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { Fumi } from 'react-native-textinput-effects';

const { connect } = require('react-redux');

import type {Customer} from '../reducers/customer';
import type {CustomerPreferences} from '../reducers/customerPreferences';
import type {Orders} from '../reducers/orders';
import type {User} from '../reducers/user';

var {USER_ID} = require('../../env');

const LaundryVroomStorage = require('LaundryVroomStorage');
const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');

import { NavigationActions } from 'react-navigation';

const backAction = NavigationActions.back({
  key: 'Landing'
})

const dismissKeyboard = require('dismissKeyboard');

var {
  authenticate,
  loadCustomerByUserId,
  loadCustomerPreferences,
  findOrders
} = require('../../actions');

class Login extends Component {
  static navigationOptions = ({navigation}) => ({
     title: 'Login',
     headerLeft:(
       <TouchableOpacity
          onPress={() =>
            navigation.navigate("Landing")
          }
          style={{"marginLeft":10}}
        >
          <FontAwesomeIcon name="chevron-left" size={30} color="#fff" />
       </TouchableOpacity>
     ),
  });

  props: {
    onLogin: () => void;
  };

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password:'',
      loginProcessing:false,
      error:false,
      errorMessage:''
    }
  }

  handleValueChange(values) {
    this.setState({ form: values });
  }

  render () {
    const {email,password} = this.state;
    const emailValid = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);
    return (
      <View style={styles.form}>
          <ScrollView keyboardShouldPersistTaps="always">
            <TouchableWithoutFeedback onPress={()=> dismissKeyboard()}>
              <View>
                {this.state.error ? (
                  <View style={styles.errorContainer}>
                    <LvText style={styles.error}>
                      {this.state.errorMessage}
                    </LvText>
                  </View>
                ) : null }
              </View>
            </TouchableWithoutFeedback>
            <View>
              <Fumi
                ref={'first'}
                returnKeyType = {"next"}
                onSubmitEditing={(event) => {
                  this.refs.SecondInput.focus();
                }}
                blurOnSubmit={false}
                label={'Email'}
                iconClass={FontAwesomeIcon}
                iconName={'envelope'}
                iconColor={LaundryVroomColors.darkBackground}
                // this is used as backgroundColor of icon container view.
                iconBackgroundColor={'#f2a59d'}
                inputStyle={{ color: LaundryVroomColors.darkText }}
                autoCapitalize='none'
                keyboardType='email-address'
                autoCorrect={false}
                onChangeText={(text) => { this.setState({email: text}) }}
              />
              <Fumi
                ref={'SecondInput'}
                returnKeyType = {"done"}
                label={'Password'}
                iconClass={MaterialIcon}
                iconName={'vpn-key'}
                iconColor={LaundryVroomColors.darkBackground}
                // this is used as backgroundColor of icon container view.
                iconBackgroundColor={'#f2a59d'}
                inputStyle={{ color: LaundryVroomColors.darkText }}
                secureTextEntry={true}
                autoCapitalize='none'
                autoCorrect={false}
                style={{ borderColor: 'gray' }}
                titleStyle={{ color: 'dimgray' }}
                inputStyle={{ color: 'slategray' }}
                onChangeText={(text) => { this.setState({password: text}) }}
                onSubmitEditing={(event) => {
                  this._login.bind(this)
                }}
              />
            </View>
            <TouchableWithoutFeedback onPress={()=> dismissKeyboard()}>
              <View>
                <View style={styles.loginButton, styles.gridItem}>
                  {this.state.loginProcessing ? (
                      <View>
                        <LaundryVroomButton
                           style={[styles.button, this.props.style, styles.buttonBottom]}
                           icon=""
                           loading={this.state.loginProcessing}
                           caption=""
                         />
                      </View>
                    ) : (
                      <View>
                        <LaundryVroomButton
                           onPress={this._login.bind(this)}
                           style={[styles.button, this.props.style, styles.buttonBottom]}
                           icon={
                            ""
                           }
                           loading={this.state.loginProcessing}
                           caption="Log In"
                         />
                       </View>
                    )
                  }
                </View>
                <View style={[styles.gridRow, styles.center,{marginTop:20}]}>
                  <LvText>Forgot Password?</LvText>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </ScrollView>
      </View>
   );
  }

  _login() {
    this.setState({loginProcessing:true, error:false});
    this.props.dispatch(authenticate(this.state.email, this.state.password)).then(function(){
      if(this.props.user && this.props.user.id) {
        this.setState({loginProcessing:false, error:false});
        this.props.dispatch(loadCustomerByUserId(this.props.user.id)).then(function() {
          if(this.props.customer && this.props.customer.id) {
            this.props.dispatch(loadCustomerPreferences(this.props.customer.id));
            this.props.dispatch(findOrders(this.props.customer.id, "PROCESSING"));
            LaundryVroomStorage.setStorageItem(USER_ID, `${this.props.customer.id}`).then(function() {
                this._goMap();
            }.bind(this));
          } else {
            if(this.props.customer && this.props.customer.message) {
              this.setState({loginProcessing:false, error:true, errorMessage: this.props.customer.message});
            } else {
               this.setState({loginProcessing:false, error:true, errorMessage: "An unknown error occurred, please try again."});
            }
          }
        }.bind(this));

      } else {
        // if(this.props.user && this.props.user.error) {}
        this.setState({loginProcessing:false, error:true, errorMessage: "Email/Password combination is incorrect"});
      }
    }.bind(this));
  }

  _goMap() {
    let {navigate} = this.props.navigation;
    navigate('Drawer');
  }
}

var styles = StyleSheet.create({
  form: {
    flex:1,
    backgroundColor:LaundryVroomColors.backgroundColor,
    opacity:1
  },
  loginForm: {
    marginBottom:20
  },
  grid: {
    margin:20
  },
  center: {
    alignSelf:'center'
  },
  gridRow: {
    flexDirection:'row',
  },
  noBottom: {
    borderStyle:'solid',
    borderBottomWidth:0,
    borderBottomColor:'white',
    borderLeftColor:'gray',
    borderLeftWidth:1,
    borderRightColor:'gray',
    borderRightWidth:1,
    borderTopColor:'gray',
    borderTopWidth:1,
  },
  fullBorder: {
    borderColor: 'gray',
    borderWidth: 1,
  },
  textInput: {
    height: 40,
    color:LaundryVroomColors.lightText,
    padding:10,
  },
  loginButton: {
    flex:1,
    marginTop:15
  },
  processingContainter: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  errorContainer: {
    alignSelf:'center',
    justifyContent: 'center'
  },
  error: {
    color: LaundryVroomColors.errorText
  }
});

function select(store) {
  return {
    user: store.user,
    customer: store.customer,
    customerPreferences: store.customerPreferences,
  };
}

module.exports = connect(select)(Login);
