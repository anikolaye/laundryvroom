/**
* @providesModule DrawerComponent
* @flow
*/

import React, {
  Component,
} from 'react';

import {
  StyleSheet,
  ScrollView,
  View
} from 'react-native';

const { connect } = require('react-redux');
import { DrawerItems } from 'react-navigation'

var { LvText } = require('LaundryVroomText');

var { USER_ID } = require('../../env');
const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomStorage = require('LaundryVroomStorage');
const Login = require('Login');
const Registration = require('Registration');

class DrawerComponent extends Component {
  props: {
    onLogin: () => void;
  };
  constructor(props) {
    super(props);
    this.state = {
      loginActive: false,
      registrationActive: false,
    };
  }
  componentWillMount() {
  }

  render () {
     return (
       <View style={styles.container}>
         <View style={styles.header}>
           <LvText style={styles.headerText}>{this.props.customer.firstName} {this.props.customer.lastName}</LvText>
         </View>
         <View>
           <DrawerItems {...this.props} />
         </View>
       </View>
     );
   }

}

var styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:"#fff",
  },
  header: {
    justifyContent:'center',
    paddingLeft:20,
    paddingTop:40,
    paddingBottom:5,
    borderBottomWidth:1,
    borderBottomColor:"#aaa",
    backgroundColor:LaundryVroomColors.darkBackground
  },
  headerText: {
    fontWeight:'bold',
    color:LaundryVroomColors.white
  }
});

function select(store) {
  return {
    customer: store.customer,
    merchantLocation:store.merchantLocation
  };
}

module.exports = connect(select)(DrawerComponent);
