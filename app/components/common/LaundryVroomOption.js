/**
* @providesModule LaundryVroomOption
* @flow
*/
import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  View,
  TextInput,
  TouchableHighlight,
  Image
} from 'react-native';

var { LvText } = require('LaundryVroomText');

const LaundryVroomColors = require('LaundryVroomColors');

import Icon from 'react-native-vector-icons/FontAwesome';

const ProductSku = {
  WASH: "WASH",
  DRYCLEAN: "DRYCLEAN"
};

class LaundryVroomOption extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value:false
    }
  }

  _renderCheckmark() {
    if (this.state.value === true) {
      return (
          <Icon style={styles.checkmark} name="check" size={30} color={LaundryVroomColors.darkBackground} />
      );
    }
    return null;
  }

  _renderImage(name) {
    if(name && name == ProductSku.WASH) {
      return (
        <Image
          style={styles.rowImage}
          resizeMode={Image.resizeMode.contain}
          source={require('../../common/images/washer.png')}
        />
      );
    } else if(name && name == ProductSku.DRYCLEAN) {
      return (
        <Image
          style={styles.rowImage}
          resizeMode={Image.resizeMode.contain}
          source={require('../../common/images/hanger_blue.png')}
        />
      );
    }
  }

  _onClose() {
    let newValue = !this.state.value;
    this.setState({value: newValue});
    this.props._onChange(newValue);
  }

  render() {
    return (
      <View style={styles.rowContainer}>
        <TouchableHighlight
          onPress={() => {this._onClose()}}
          underlayColor={'#c7c7cc'}
          {...this.props} // mainly for underlayColor
        >
          <View style={styles.row}>
            {this._renderImage(this.props.name)}
            <LvText style={styles.switchTitle}>
              {this.props.title}
            </LvText>
            {this._renderCheckmark()}
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  rowImage: {
    height:90,
    width:90,
    marginLeft:10
  },
  rowContainer: {
    borderWidth:1,
    borderColor:'#c7c7cc',
    margin:10
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height:100
  },
  checkmark: {
    width: 40,
    marginRight: 10,
    marginLeft: 10
  },
  switchTitle: {
    fontFamily:LaundryVroomColors.fontFamily,
    fontSize: 15,
    color: '#000',
    flex: 0.7,
    paddingLeft: 10,
  },
});

module.exports = LaundryVroomOption;
