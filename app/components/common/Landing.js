/**
* @providesModule Landing
* @flow
*/
import React, {
  Component
} from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Modal,
  ScrollView,
  Dimensions
} from 'react-native';

var { LvText } = require('LaundryVroomText');

var { USER_ID } = require('../../env');

const LaundryVroomStorage = require('LaundryVroomStorage');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomColors = require('LaundryVroomColors');

const Login = require('Login');
const Registration = require('Registration');


const CARD_PREVIEW_WIDTH = 20
const CARD_MARGIN = 5;
const CARD_WIDTH = Dimensions.get('window').width - (CARD_MARGIN + CARD_PREVIEW_WIDTH) * 2;

class Landing extends Component {
  static navigationOptions = ({navigation}) => ({
    header:null
  });
  props: {
  };
  state: {
    loginActive: boolean,
    registrationActive: boolean
  };
  constructor(props) {
    super(props);
    this.state = {
      loginActive: false,
      registrationActive: false,
    };
  }

  _loginNav() {
    const {navigate} = this.props.navigation;
    navigate('LoginScreen');
  }

  _registrationNav() {
    const {navigate} = this.props.navigation;
    navigate('RegisterScreen');
  }

  render () {
     return (
       <View style={{flex:1}}>
      	 <View style={styles.scrollable}>
           <ScrollView
               style={styles.container}
               automaticallyAdjustInsets={false}
               horizontal={true}
               decelerationRate={0}
               snapToInterval={CARD_WIDTH + CARD_MARGIN*2}
               snapToAlignment="start"
               contentContainerStyle={styles.content}
                >
                  <View style={styles.card}>
                    <LvText>Welcome to LaundryVroom!  We are an On-Demand Laundry Delivery Service.</LvText>
                  </View>
                  <View style={styles.card}>
                    <LvText>No need to schedule, we come right away to get your laundry done fast with the quality of your local laundromat.</LvText>
                  </View>
                  <View style={styles.card}>
                    <LvText>We partner with local laundromats near you to ensure you get the best quality.</LvText>
                  </View>
                  <View style={styles.card}>
                    <LvText>No hassle, get your weekends back. Sign in and get your laundry done now!</LvText>
                  </View>
          </ScrollView>
        </View>
        <View style={[styles.grid, styles.gridRowBottom]}>
           <View style={styles.gridItem}>
               <LaundryVroomButton
                  onPress={this._loginNav.bind(this)}
                  style={[styles.button, this.props.style, styles.buttonBottom]}
                  caption="Sign In"
                />
          </View>
           <View style={styles.gridItem}>
                <LaundryVroomButton
                   onPress={this._registrationNav.bind(this)}
                   style={[styles.button, this.props.style, styles.buttonBottom]}
                   caption="Register"
                 />
           </View>
         </View>
       </View>
     );
   }

}

var styles = StyleSheet.create({
  grid: {
    flex:1,
    flexDirection:'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin:0
  },
  gridRow: {
    flex:1,
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridRowBottom: {
    flex:1,
    flexDirection:'row',
    alignItems: 'flex-end',
  },
  gridItem: {
    alignItems: 'center',
    justifyContent: 'center',
    padding:20
  },
  buttonBottom: {
    width:125
  },
  scrollable: {
    flex:4,
    flexDirection:'column'
  },
  container: {
    flex: 4,
    backgroundColor: '#F5FCFF',
    flexDirection:'row'
  },
  content: {
    marginTop: 20,
    paddingHorizontal: CARD_PREVIEW_WIDTH,
    alignItems: 'center'
  },
  card: {
    // flex: 1,
    backgroundColor: LaundryVroomColors.lightestBackground,
    width: CARD_WIDTH,
    margin: CARD_MARGIN,
    height: CARD_WIDTH,
    padding:CARD_MARGIN,
    borderColor:'#ececec',
    borderWidth:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

module.exports = Landing;
