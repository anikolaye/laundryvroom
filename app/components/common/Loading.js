/**
* @providesModule Loading
* @flow
*/

import React, {
  Component,
} from 'react';

import {
  ActivityIndicator,
  StyleSheet,
  View,
  TouchableOpacity,
  Modal,
  Image
} from 'react-native';


var { LvText } = require('LaundryVroomText');

var { USER_ID } = require('../../env');
const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomStorage = require('LaundryVroomStorage');
const Login = require('Login');
const Registration = require('Registration');

class Loading extends Component {
  props: {
    onLogin: () => void;
  };
  constructor(props) {
    super(props);
    this.state = {
      loginActive: false,
      registrationActive: false,
    };
  }
  componentWillMount() {
  }

  render () {
     return (
       <View style={styles.grid}>
          <ActivityIndicator animating={true} color={LaundryVroomColors.darkBackground} size="large"/>
          <LvText>Loading...</LvText>
       </View>
     );
   }

}

var styles = StyleSheet.create({
  grid: {
    flex:1,
    flexDirection:'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin:0
  },
  gridRow: {
    flex:1,
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridRowBottom: {
    flex:1,
    flexDirection:'row',
    alignItems: 'flex-end',
  },
  gridItem: {
    alignItems: 'center',
    justifyContent: 'center',
    padding:20
  },
  buttonBottom: {
    width:125
  }
});

module.exports = Loading;
