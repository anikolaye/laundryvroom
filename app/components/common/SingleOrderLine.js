/**
* @providesModule SingleOrderLine
* @flow
*/

import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ListView
} from 'react-native';

var { LvText } = require('LaundryVroomText');

import type { Dispatch } from '../actions/types';

const LaundryVroomColors = require('LaundryVroomColors');
const LaundryVroomButton = require('LaundryVroomButton');
const LaundryVroomToolbar = require('LaundryVroomToolbar');

class SingleOrderLine extends Component {
  props: {
    orderLine: any;
  };

  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      orderLineProductAttributes: ds.cloneWithRows({}),
    }
  }

  componentDidMount() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.setState({orderLineProductAttributes: ds.cloneWithRows(this.props.orderLine.orderLineProduct.orderLineProductAttributes)});
  }

  render () {
    let {orderLine} = this.props;
    const {orderLineProduct} = orderLine;

    return (
      <View>
        <View style={styles.row}>
          <View style={styles.imageContainer}>
            <LvText numberOfLines={1} style={styles.name}>
              {orderLineProduct.name}
            </LvText>
            {orderLineProduct.sku == 'WASH' ? (
              <Image
               style={styles.thumbnail}
               resizeMode={Image.resizeMode.contain}
               source={require('../../common/images/washer.png')}
              />
            ) :
              <Image
               style={styles.thumbnail}
               resizeMode={Image.resizeMode.contain}
               source={require('../../common/images/hanger_blue.png')}
              />
            }
          </View>
          <View style={styles.deliveryContainer}>
            <LvText style={styles.name}>
              Sending To
            </LvText>
            <LvText style={styles.storeName}>
              {orderLine.destinationName}
            </LvText>
            <LvText style={styles.addressLine}>
              {orderLine.destinationAddress.addressLine1}
            </LvText>
            <LvText style={styles.addressCity}>
             {orderLine.destinationAddress.city}, {orderLine.destinationAddress.state} {orderLine.destinationAddress.zip}
            </LvText>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.attributeContainer}>
            <ListView
              style={styles.ordersContainer}
              dataSource={this.state.orderLineProductAttributes}
              renderRow={this._renderRow.bind(this)}
              enableEmptySections={true}
            />
          </View>
        </View>
      </View>
   );
  }

  _renderRow(orderLineProductAttribute) {
    return(
      <View style={styles.attribute} >
        <LvText>{orderLineProductAttribute.key} - {orderLineProductAttribute.value}</LvText>
      </View>
    );
  }

  _goBack() {
    this.props.navigator.pop();
  }
}

var styles = StyleSheet.create({
  row: {
    flex:1,
    backgroundColor:LaundryVroomColors.backgroundColor,
    opacity:1,
    flexDirection:'row',
    justifyContent:'space-between',
  },
  imageContainer: {
    marginTop:20,
    // alignSelf:'flex-start',
    // justifyContent: 'flex-start'
  },
  deliveryContainer: {
    marginTop:20,
    marginLeft:10,
    width:275,
    // alignSelf:'flex-start',
    // justifyContent: 'flex-start'
  },
  attributeContainer: {
    marginTop:20,
    // flex:1,
    // flexDirection:'column',
    // margin:25,
    // width:200,
    // height:100,
    // alignSelf:'flex-end',
    // justifyContent: 'flex-end'
  },
  textInput: {
    height: 40,
    color:LaundryVroomColors.lightText,
    padding:10,
  },
  thumbnail: {
    height:60,
    width:60
  },
  name: {
    paddingBottom:5,
    fontWeight:'bold'
  },
  attribute: {

  }
});

module.exports = SingleOrderLine;
