package com.laundryvroom;

import com.facebook.react.ReactActivity;
import com.airbnb.android.react.maps.MapsPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import cl.json.RNSharePackage;
import com.burnweb.rnsendintent.RNSendIntentPackage;
import com.AirMaps.AirPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.devfd.RNGeocoder.RNGeocoderPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "LaundryVroom";
    }
}
